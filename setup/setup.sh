#!/bin/bash
#
if [ "$EUID" -ne 0 ]
  then echo 'Please run as root'
  exit
fi
# cd /var/www/setup
# sudo chmod 700 setup.sh
# sudo ./setup.sh
echo 'Setup Menu'
echo '1 - install (L)AMP on xenial/64 box (dev)'
echo '2 - install HTTPS with apache2'
echo '3 - install URL Rewrite with apache2'
echo '4 - refresh mysql database (for all apps)'
# echo '10 - install LAMP Stack on production'
# echo '11 - install Apache with Php on production'
# echo '12 - install MySql on production'
read choice

case $choice in
    1)  apt update
    apt install -y apache2
    # back up the current settings, copy in our own settings
    cp /etc/apache2/apache2.conf /etc/apache2/apache2.conf.bak
cat <<START >> "/etc/apache2/apache2.conf"

ServerName 192.168.33.10
START
    apt install -y mysql-server # root, root
    apt install -y php php-mysql libapache2-mod-php php-curl php-mcrypt
    ufw enable # enable the firewall
    ufw allow in "Apache Full" # firewall allow: 80,443/tcp
    ufw allow ssh # firewall allow: ssh 22
    ufw default deny # deny other traffic
    service apache2 restart # restart apache
    echo 'Enter the root mysql password';
    mysql -u root -p < /var/www/examples/mysql/framework.sql
    ;;
    # use X.509 certificate signing request (CSR) management # valid for 3 years
    2) openssl req -x509 -nodes -days 1095 -newkey rsa:2048 -keyout /etc/ssl/private/apache-selfsigned.key -out /etc/ssl/certs/apache-selfsigned.crt
    # echo 'Country Name (2 letter code) [AU]:US'
    # echo 'State or Province Name (full name) [Some-State]:New York'
    # echo 'Locality Name (eg, city) []:NYC'
    # echo 'Organization Name (eg, company) [Internet Widgits Pty Ltd]:Widgets LLC'
    # echo 'Organizational Unit Name (eg, section) []:Department'
    # echo 'Common Name (e.g. server FQDN or YOUR name) []:192.168.33.10'
    # echo 'Email Address []:admin@192.168.33.10'
    openssl dhparam -out /etc/ssl/certs/dhparam.pem 2048
    cp /var/www/examples/apache/ssl-params.conf /etc/apache2/conf-available/ssl-params.conf
    # back up the current settings, copy in our own settings
    mv /etc/apache2/sites-available/default-ssl.conf /etc/apache2/sites-available/default-ssl.conf.bak
    cp /var/www/examples/apache/default-ssl.conf /etc/apache2/sites-available/default-ssl.conf
    # back up the current settings, copy in our own settings
    mv /etc/apache2/sites-available/000-default.conf /etc/apache2/sites-available/000-default.conf.bak
    cp /var/www/examples/apache/000-default.conf /etc/apache2/sites-available/000-default.conf
    ufw allow "OpenSSH" # firewall allow: SSH connections
    # enable mod_ssl, the Apache SSL module, and mod_headers
    a2enmod ssl
    a2enmod headers
    a2ensite default-ssl # enable our SSL Virtual Host
    a2enconf ssl-params # enable our ssl-params.conf file
    apachectl restart # restart apache
    ;;
    3) a2enmod rewrite
    rm /etc/apache2/apache2.conf # backup already exists @ /var/www/examples/apache/apache2.conf
    cp /var/www/examples/apache/a2enmod-rewrite.conf /etc/apache2/apache2.conf
    /etc/init.d/apache2 restart
    ;;
    4) echo 'Which app(s) would you like to refresh (type "exit" to exit)';
    read app
    while [ "$app" != "exit" ]
    do
        echo 'Enter the root mysql password';
        mysql -u root -p < "/var/www/framework/apps/${app}/examples/mysql/${app}.sql"
        read app
    done
    # echo 'Enter the root mysql password';
    # mysql -u root -p < /var/www/examples/mysql/framework.sql
    # for each app installed, import that raw mysql file
    # mysql -u root -p < /var/www/framework/apps/{$app}/mysql/{$app}.sql
    ;;
    5) echo 'Method coming soon';
    ;;
    6) echo 'Method coming soon';
    ;;
    *) echo 'Error! please enter 1, 2, 3, 4, 5 or 6';;
esac
