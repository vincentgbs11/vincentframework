<?php
/* Check that config file is set */
if (is_file('../framework/config.php')) {
    include_once('../framework/config.php');
} else {
    exit('500 Internal Server Error: Missing configuration file.');
}
/* DEBUGGING mode is required to be set, from config file */
if (DEBUG == 'ON') {
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
} else if (DEBUG == 'OFF') {
    error_reporting(E_ERROR);
} else {
    exit('500 Internal Server Error: Debug mode not set');
}
/* Keep logs of page and memory usage */
if (LOG == 'ON') {
    $time_start = microtime(true);
}
/* Get the desired url from a $_GET request, can use a rewrite engine for pretty urls */
if (isset($_GET['url']) && $_GET['url'] !== '') {
    $url = filter_input(INPUT_GET, 'url', FILTER_SANITIZE_STRING);
    $url = strtolower(preg_replace("/[^a-zA-Z0-9\/]/", '', $url));
} else {
    $url = HOME;
}
/* shortController checks for single param from $_GET, routes to shortcut */
$route = explode('/', $url);
if (count($route) == 1) {
    include_once FILE . 'framework/apps/short/controller/shortController.php';
    $controller = new shortController();
    $url = $controller->shortcut($route[0]);
    $route = explode('/', $url);
    $controller->shortModel->db->close();
} // continue
/* Check file exists, controller exists, module is enabled */
define('URL', $url);
if (isset($route[0], $route[1]) && is_file(FILE . "framework/apps/{$route[0]}/controller/{$route[1]}Controller.php")) {
    if (in_array($route[0], MODULES)) {
        $controller = $route[1] . 'Controller';
        include_once FILE . "framework/apps/{$route[0]}/controller/{$controller}.php";
        if (class_exists($controller)) {
            $controller = new $controller();
        } else {
            exit('404 Not Found: Missing controller class');
        }
    } else {
        exit('405 Not Allowed: This module is disabled. Please contact a system administrator.');
    }
} else {
    exit('404 Not Found: Missing controller file');
}
/* Check function exists, call function or default home() */
if (isset($route[2]) && method_exists($controller, $route[2])) {
    $function = $route[2];
    $controller->$function();
} else {
    $controller->home();
}
/* Log page and memory usage */
if (LOG == 'ON') {
    $time_end = microtime(true);
    $execution_time = number_format($time_end - $time_start, 3);
    $log = '../framework/log.txt';
    $lines = count(file($log));
    if ($lines > 9999) {
        $open = 'w';
    } else {
        $open = 'a';
    }
    $fp = fopen($log, $open);
    fwrite($fp, URL . ': ' . number_format(memory_get_peak_usage(1))
        . ' bytes and '. $execution_time . ' seconds from '
        . $_SERVER['REMOTE_ADDR'] . PHP_EOL);
    fclose($fp);
}
?>
