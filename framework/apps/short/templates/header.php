<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="author" content="">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Shortcut Controller</title>
</head>
<body class="container">
    <div id="noscript_lightbox">
        <div class="text-center bg-primary lead">Javascript not loaded. Your browser may not support JavaScript.</div>
    </div>
    <div id="flash_message"></div>

    <link rel="stylesheet" type="text/css" href="/css/library/4.1.3.bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/css/library/1.12.1.jquery-ui.min.css">
    <script src="/js/library/jquery-3.2.1.min.js"
            integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
            crossorigin="anonymous"></script>
    <script src="/js/library/1.12.1.jquery-ui.min.js"
            type="text/javascript"></script>
    <script src="/js/library/sha256.js" type="text/javascript"></script>
    <script src="/js/vanilla.js" type="text/javascript"></script>
