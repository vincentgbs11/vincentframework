<?php $this->js('admin/shortcuts'); ?>
<div class="row col-12" id="shortcuts_form">
    <div class="row col-12 form-group">
        <label for="shortcut">Add shortcut</label>
        <input type="text" class="form-control" id="shortcut" aria-describedby="shortcutHelp" maxlength="99" placeholder="shortcut">
        <input type="text" class="form-control" id="destination" maxlength="255" placeholder="destination">
        <button class="btn btn-outline-dark" id="shortcut_button">Add shortcut</button>
        <small id="shortcutHelp" class="form-text text-muted">
            &#160;Shortcut format: "xxxxxxxxxx"; Destination format: xxxxx/xxxxx/xxxxx
        </small>
    </div> <!-- </div class="row col-12 form-group"> -->
</div> <!-- </div class="row col-12" id="shortcuts_form"> -->
<table class="table" id="shortcuts_table">
    <caption>A list of all website shortcuts</caption>
    <thead class="table-active">
        <tr>
            <td>shortcut_id</td>
            <td>shortcut</td>
            <td>destination</td>
            <td>delete</td>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($data AS $shortcut) {
            echo "<tr><td>{$shortcut->shortcut_id}</td>
                <td>{$shortcut->shortcut}</td>
                <td>{$shortcut->destination}</td>
                <td><button shortcut='{$shortcut->shortcut_id}'
                    class='delete_shortcut btn btn-warning'>delete</button></td></tr>";
        } ?>
    </tbody>
</table>
