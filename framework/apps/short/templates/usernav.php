<div class="row col-12 nav_bar">
    <a href="/"><button class="btn btn-outline-dark">Home</button></a>
    <a href="<?php $this->url('user/user/update') ?>"><button class="btn btn-outline-dark">Update Account</button></a>
    <a href="<?php $this->url('user/user/register') ?>"><button class="btn btn-outline-dark">Register</button></a>
    <a href="<?php $this->url('user/user/login') ?>"><button class="btn btn-outline-dark">Login</button></a>
    <a href="<?php $this->url('user/user/logout') ?>"><button class="btn btn-outline-dark">Logout</button></a>
    <a href="<?php $this->url('user/user/reset') ?>"><button class="btn btn-outline-dark">Reset Password</button></a>
    <a href="<?php $this->url('user/user/deactivate') ?>"><button class="btn btn-outline-dark">Deactivate</button></a>
</div> <!-- </div class="row col-12"> -->
