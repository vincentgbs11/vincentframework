<?php
require_once FILE . 'framework/system/view.php';

class shortView extends view {

    public function __construct($app)
    {
        parent::__construct($app);
        $this->loadTemplate('header', NULL, 'header');
        $this->loadTemplate('usernav', NULL, 'header');
        $this->loadTemplate('footer', NULL, 'footer');
    }

    public function admin($shortcuts)
    {
        $this->loadTemplate('admin', $shortcuts);
        return $this->display();
    }

}
?>
