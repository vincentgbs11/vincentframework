$(document).ready(function(){
    let admin = {
        g: {
            limitInput: function(input, limit) {
                {
                    if (input.length <= 0) { return; }
                    if (limit == 'permission') {
                        $(input).val( $(input).val().replace(/[^A-z/*]/g, '') );
                        $(input).val( $(input).val().replace(/\^/g, '') );
                    } else if (limit == 'group' || limit == 'shortcut') {
                        $(input).val( $(input).val().replace(/[^A-z]/g, '') );
                        $(input).val( $(input).val().replace(/\^/g, '') );
                    } else if (limit == 'user') {
                        $(input).val( $(input).val().replace(/[^A-z0-9]/g, '') );
                        $(input).val( $(input).val().replace(/\^/g, '') );
                    } else {
                        console.debug('Invalid input type');
                    }
                }
            }, // limitInput
            flashMessage: function(message, time = 5000, color = ['09F', '000']) {
                if ($("#flash_message").length <= 0) { return; }
                $("#flash_message").attr('style', 'position:absolute;z-index:9;background-color:#' + color[0] + ';color:#' + color[1]);
                $("#flash_message").html(message);
                setTimeout(
                    function() {
                        $("#flash_message").attr('style', ''); // clear style
                        $("#flash_message").html(''); // clear text
                    }, // function
                    time
                ); // setTimeout
            } // flashMessage
        },
        shortcuts: {
            addShortcut: function(shortcut, destination, table){
                shortcut = shortcut.val();
                destination = destination.val();
                if (shortcut.match('([A-z]+)') && destination.match('([A-z]+\/[A-z]+\/[A-z]+)')) {
                    $.ajax({
                        url: "?url=user/admin/shortcuts",
                        type: "POST",
                        data: {
                            add_shortcut: shortcut,
                            destination: destination
                        },
                        success: function(response) {
                            try {
                                shortcut = JSON.parse(response);
                                $('#'+table+' tr:last').after('<tr><td>'+shortcut.shortcut_id+'</td><td>'+shortcut.shortcut+'</td><td>'+shortcut.destination+'</td><td><button shortcut='+shortcut.shortcut_id+' class="delete_shortcut btn btn-warning">delete</button></td></tr>');
                                return admin.g.flashMessage('Shortcut added');
                            } catch(e) {
                                console.debug(e);
                                return admin.g.flashMessage(response);
                            }
                        } // success
                    }); // ajax
                } else {
                    return admin.g.flashMessage('Invalid shortcut or destination format');
                }
            }, // addShortcut function
            deleteShortcut: function(button) {
                shortcut = parseInt(button.attr('shortcut'));
                if (shortcut > 0) {
                    $.ajax({
                        url: "?url=user/admin/shortcuts",
                        type: "POST",
                        data: {
                            delete_shortcut: shortcut
                        },
                        success: function(response) {
                            if (response.trim() == 'Shortcut deleted') {
                                button.closest('tr').remove();
                            }
                            return admin.g.flashMessage(response);
                        } // success
                    }); // ajax
                }
            } // deleteShortcut function
        } // shortcut functions
    };


    $('#shortcut').keyup(function(){
        return admin.g.limitInput($('#shortcut'), 'shortcut');
    });

    $("#shortcut_button").on('click', function(){
        return admin.shortcuts.addShortcut($('#shortcut'), $('#destination'), 'shortcuts_table');
    });

    $("#shortcuts_table").on('click', ".delete_shortcut.btn-danger", function(){
        return admin.shortcuts.deleteShortcut($(this));
    });
});
