<?php
require_once FILE . 'framework/system/model.php';

class shortModel extends model {

    public function selectShortcut($short)
    {
        $q = "SELECT `destination` FROM `short_ls_shortcuts` WHERE `shortcut`='{$short}';";
        $short = $this->select($q);
        if (isset($short[0])) {
            return $short[0]->destination;
        } else {
            exit ('<h2>Invalid url</h2><a href="?url='.HOME.'">Return to site</a>');
        }
    }

    public function selectShortcuts($like=false, $lim=99)
    {
        $q = 'SELECT `shortcut_id`, `shortcut`, `destination` FROM `short_ls_shortcuts`';
        if ($like) {
            $q .= " WHERE `shortcut` LIKE '{$like}%'";
        }
        $q .= " LIMIT {$lim};";
        return $this->select($q);
    }

    public function createShortcut($shortcut)
    {
        $q = "INSERT INTO `short_ls_shortcuts` (`shortcut`, `destination`)
            VALUES ({$this->wrap($shortcut['shortcut'])}, {$this->wrap($shortcut['destination'])});";
        return $this->execute($q);
    }

    public function deleteShortcut($shortcut)
    {
        $q = 'DELETE FROM `short_ls_shortcuts` WHERE ';
        foreach ($shortcut as $key => $value) {
            $q .= " `{$key}`={$this->wrap($value)} AND";
        }
        $q = substr($q, 0, -4) . 'LIMIT 1;';
        return $this->execute($q);
    }

}
?>
