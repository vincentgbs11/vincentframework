<?php
require_once FILE . 'framework/system/controller.php';

class shortController extends controller {

    public function __construct()
    {
        parent::__construct('short', 'short', false);
        $this->getModel('short', 'short', false);
        $this->getView('short');
    }

    public function shortcut($shortcut)
    {
        return $this->shortModel->selectShortcut($shortcut);
    }

    public function admin()
    {
        $this->getController('user', 'user', $this->shortModel->db);
        if ($this->userController->check()) {
            if (isset($_POST['add_shortcut'], $_POST['destination'])) {
                $shortcut['shortcut'] = strtolower($this->post('add_shortcut', 'l', 99));
                $shortcut['destination'] = strtolower($this->post('destination', 's', 255, '/'));
                if ($this->shortModel->createShortcut($shortcut)) {
                    $shortcut = $this->shortModel->selectShortcuts($shortcut['shortcut'], 1);
                    echo json_encode($shortcut[0]);
                    return true;
                } else {
                    exit('Error adding shortcut');
                }
            } elseif (isset($_POST['delete_shortcut'])) {
                $shortcut['shortcut_id'] = $this->post('delete_shortcut', 'i', 11);
                if ($this->shortModel->deleteShortcut($shortcut)) {
                    exit('Shortcut deleted');
                } else {
                    exit('Error deleting shortcut');
                }
            }
            $shortcuts = $this->shortModel->selectShortcuts();
            return $this->shortView->admin($shortcuts);
        }
    }

}
?>
