<?php
require_once FILE . 'framework/system/view.php';

class adminView extends view {

    public function __construct($app)
    {
        parent::__construct($app);
        $this->loadTemplate('admin/header', NULL, 'header');
        $this->loadTemplate('admin/footer', NULL, 'footer');
    }

    public function home()
    {
        return $this->display();
    }

    public function permissions($permissions)
    {
        $this->loadTemplate('admin/permissions', $permissions);
        return $this->display();
    }

    public function groups($groups)
    {
        $this->loadTemplate('admin/groups', $groups);
        return $this->display();
    }

    public function users($users)
    {
        $this->loadTemplate('admin/users', $users);
        return $this->display();
    }

    public function settings($settings)
    {
        $this->loadTemplate('admin/settings', $settings);
        return $this->display();
    }

}
?>
