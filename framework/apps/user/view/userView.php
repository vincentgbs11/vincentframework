<?php
require_once FILE . 'framework/system/view.php';

class userView extends view {

    public function __construct($app)
    {
        parent::__construct($app);
        $this->loadTemplate('user/header', NULL, 'header');
        $this->loadTemplate('user/footer', NULL, 'footer');
    }

    public function register($codes=['referral'=>'', 'signupcode'=>''])
    {
        $this->loadTemplate('user/register', $codes);
        return $this->display();
    }

    public function terms()
    {
        $this->loadTemplate('user/terms');
        return $this->display();
    }

    public function login()
    {
        $this->loadTemplate('user/login');
        return $this->display();
    }

    public function home($user)
    {
        $this->loadTemplate('user/home', $user);
        return $this->display();
    }

    public function request()
    {
        $this->loadTemplate('user/request');
        return $this->display();
    }

    public function reset($resetCode)
    {
        $this->loadTemplate('user/reset', $resetCode);
        return $this->display();
    }

    public function deactivate($user)
    {
        $this->loadTemplate('user/deactivate', $user);
        return $this->display();
    }

    public function update($user)
    {
        $this->loadTemplate('user/update', $user);
        return $this->display();
    }

    public function google($settings=false)
    {
        if ($settings) {
            $this->loadTemplate('user/google/glogin', $settings);
            return $this->display(false);
        } else {
            $this->loadTemplate('user/google/gregister');
            return $this->display();
        }
    }

}
?>
