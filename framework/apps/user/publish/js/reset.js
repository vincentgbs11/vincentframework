$(document).ready(function() {
    $("#reset_form").on('click', "#toggle_password", function(){
        user.register.toggle($("#password"));
        user.register.toggle($("#confirm"));
    });

    $("#password_button").on('click', function(){
        user.g.generatePassword($('#password'), $('#confirm'));
    });

    $("#reset_button").on('click', function(){
        user.resetPassword($("#reset_code"), $("#username"), $("#password"), $("#confirm"));
    });
    $("#password, #confirm").keydown(function(e){
        if (e.keyCode == 13) { // enter
            user.resetPassword($("#reset_code"), $("#username"), $("#password"), $("#confirm"));
        }
    });
});
