$(document).ready(function(){
    $('#group').keyup(function(){
        admin.g.limitInput($('#group'), 'group');
    });

    $("#group_button").on('click', function(){
        admin.groups.addGroup($('#group'), $('#add_to_group'));
    });

    $("#groups_form").on('click', "#delete_group.btn-danger", function(){
        admin.groups.deleteGroup($("#add_to_group"));
    });

    $("#permission_button").on('click', function(){
        admin.groups.addPermissionToGroup($("#add_to_permission"), $("#add_to_group"), 'groups_table');
    });

    $("#groups_table").on('click', ".remove_permission.btn-danger", function(){
        admin.groups.removePermissionFromGroup($(this));
    });
});
