$(document).ready(function(){
    $('#permission').keyup(function(){
        admin.g.limitInput($('#permission'), 'permission');
    });

    $("#permission_button").on('click', function(){
        admin.permissions.addPermission($('#permission'), 'permissions_table');
    });

    $("#permissions_table").on('click', ".delete_permission.btn-danger", function(){
        admin.permissions.deletePermission($(this));
    });
});
