var admin = {
    g: {
        limitInput: function(input, limit) {
            {
                if (input.length <= 0) { return; }
                if (limit == 'permission') {
                    $(input).val( $(input).val().replace(/[^A-z/*]/g, '') );
                    $(input).val( $(input).val().replace(/\^/g, '') );
                } else if (limit == 'group' || limit == 'shortcut') {
                    $(input).val( $(input).val().replace(/[^A-z]/g, '') );
                    $(input).val( $(input).val().replace(/\^/g, '') );
                } else if (limit == 'user') {
                    $(input).val( $(input).val().replace(/[^A-z0-9]/g, '') );
                    $(input).val( $(input).val().replace(/\^/g, '') );
                } else {
                    console.debug('Invalid input type');
                }
            }
        }, // limitInput
        flashMessage: function(message, time = 5000, color = ['09F', '000']) {
            if ($("#flash_message").length <= 0) { return; }
            $("#flash_message").attr('style', 'position:absolute;z-index:9;background-color:#' + color[0] + ';color:#' + color[1]);
            $("#flash_message").html(message);
            setTimeout(
                function() {
                    $("#flash_message").attr('style', ''); // clear style
                    $("#flash_message").html(''); // clear text
                }, // function
                time
            ); // setTimeout
        } // flashMessage
    }, // general functions
    permissions: {
        addPermission: function(input, table) {
            var permission = input.val();
            if (permission.match('([A-z]+\/[A-z]+\/[A-z*]+)')) {
                $.ajax({
                    url: "?url=user/admin/permissions",
                    type: "POST",
                    data: {
                        add_permission: permission
                    },
                    success: function(response) {
                        if (response && (parseInt(JSON.parse(response)['permission_id']) > 0)) {
                            permission = JSON.parse(response);
                            $('#'+table+' tr:last').after('<tr><td>'+permission.permission_id+'</td><td>'+permission.function+'</td><td><button permission='+permission.permission_id+' class="delete_permission btn btn-warning">delete</button></td></tr>');
                            return admin.g.flashMessage('Permissions added');
                        } else {
                            return admin.g.flashMessage(response);
                        }
                    } // success
                }); // ajax
            } else {
                return admin.g.flashMessage('Missing or invalid permission format');
            }
        }, // addPermission
        deletePermission: function(input) {
            var permission = input.attr('permission');
            $.ajax({
                url: "?url=user/admin/permissions",
                type: "POST",
                data: {
                    delete_permission: permission
                },
                success: function(response) {
                    if (response && response.trim() == 'Deleted') {
                        input.closest('tr').remove();
                    }
                    return admin.g.flashMessage(response);
                } // success
            }); // ajax
        } // deletePermission function
    }, // permissions functions
    groups: {
        addGroup: function(input, groupList) {
            var group = input.val();
            if (group.match('([A-z]+)')) {
                $.ajax({
                    url: "?url=user/admin/groups",
                    type: "POST",
                    data: {
                        add_group: group
                    },
                    success: function(response) {
                        try {
                            group = JSON.parse(response);
                            groupList.append($('<option></option>').attr('group', group.group_id).text(group.group_name));
                            return admin.g.flashMessage(group.group_name + ' added');
                        } catch(e) {
                            console.debug(e);
                            return admin.g.flashMessage(response);
                        }
                    } // success
                }); // ajax
            } else {
                return admin.g.flashMessage('Missing or invalid group format');
            }
        }, // addGroup
        deleteGroup: function(groupList) {
            var group = groupList.find(':selected');
            $.ajax({
                url: "?url=user/admin/groups",
                type: "POST",
                data: {
                    remove_group: group.attr('group')
                },
                success: function(response) {
                    if (response.trim() == 'Deleted') {
                        group.remove();
                    }
                    return admin.g.flashMessage(response);
                } // success
            }); // ajax
        }, // deleteGroup function
        addPermissionToGroup: function(permissionList, groupList, table) {
            var selected = permissionList.find(':selected');
            var permissions = '';
            selected.each(function( i ) {
                permissions += $(this).attr('permission') + ',';
            });
            permissions = permissions.slice(0,-1);
            var group = groupList.find(':selected').attr('group');
            if ((parseInt(group) > 0) && (permissions != '')) {
                $.ajax({
                    url: "?url=user/admin/groups",
                    type: "POST",
                    data: {
                        add_permission: permissions,
                        group: group
                    },
                    success: function(response) {
                        try {
                            permissions = JSON.parse(response);
                            $(permissions).each(function( i ) {
                                $('#'+table+' tr:last').after('<tr><td>'+permissions[i].group_id+'</td><td>'+permissions[i].group_name+'</td><td>'+permissions[i].permission_id+'</td><td>'+permissions[i].function+'</td><td><button group='+permissions[i].group_id+' permission='+permissions[i].permission_id+' class="delete_group btn btn-warning">delete</button></td></tr>');
                            });
                            return admin.g.flashMessage('Permissions added to group');
                        } catch(e) {
                            console.debug(e);
                            return admin.g.flashMessage(response);
                        }
                    } // success
                }); // ajax
            } else {
                return admin.g.flashMessage('Missing or invalid group format OR Unselected permissions');
            }
        }, // addPermissionToGroup function
        removePermissionFromGroup: function(button) {
            var permission = button.attr('permission');
            var group = button.attr('group');
            $.ajax({
                url: "?url=user/admin/groups",
                type: "POST",
                data: {
                    remove_permission: permission,
                    group: group
                },
                success: function(response) {
                    if (response.trim() == 'Permission deleted from group') {
                        button.closest('tr').remove();
                    }
                    return admin.g.flashMessage(response);
                } // success
            }); // ajax
        } // removePermissionFromGroup function
    }, // groups
    users: {
        addUser: function(username, email, password) {
            var username = username.val();
            var email = email.val();
            var password = password.val();
            var regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if (username.match('([A-z0-9]+)') && regex.test(email)) {
                $.ajax({
                    url: "?url=user/admin/users",
                    type: "POST",
                    data: {
                        add_user: username,
                        email: email,
                        password: SHA256(password)
                    },
                    success: function(response) {
                        return admin.g.flashMessage(response);
                    } // success
                }); // ajax
            } else {
                return admin.g.flashMessage('Missing or invalid username, email or password');
            }
        }, // addUser function
        addUserToGroup: function(user, group, table) {
            user = parseInt(user.find(':selected').attr('user'));
            group = parseInt(group.find(':selected').attr('group'));
            if (user > 0 && group > 0) {
                $.ajax({
                    url: "?url=user/admin/users",
                    type: "POST",
                    data: {
                        add_group: group,
                        user: user
                    },
                    success: function(response) {
                        try {
                            user = JSON.parse(response);
                            $('#'+table+' tr:last').after('<tr><td>'+user.user_id+'</td><td>'+user.username+'</td><td>'+user.email+'</td><td>'+user.registration_date+'</td><td>'+user.group_id+'</td><td>'+user.group_name+'</td><td><button user='+user.user_id+' group='+user.group_id+' class="remove_user_group btn btn-warning">delete</button></td></tr>');
                            return admin.g.flashMessage('User added to group');
                        } catch(e) {
                            console.debug(e);
                            return admin.g.flashMessage(response);
                        }
                    } // success
                }); // ajax
            } else {
                return admin.g.flashMessage('Missing username or group');
            }
        }, // addUserToGroup function
        removeUserFromGroup: function(button) {
            user = parseInt(button.attr('user'));
            group = parseInt(button.attr('group'));
            if (user > 0 && group > 0) {
                $.ajax({
                    url: "?url=user/admin/users",
                    type: "POST",
                    data: {
                        remove_group: group,
                        user: user
                    },
                    success: function(response) {
                        if (response.trim() == 'User removed from group') {
                            button.closest('tr').remove();
                        }
                        return admin.g.flashMessage(response);
                    } // success
                }); // ajax
            } else {
                return admin.g.flashMessage('Missing username or group');
            }
        } // removeUserFromGroup function
    }, // users
} // admin Object

$(document).ready(function() {
    $(document).on('click', ".btn-warning", function(e){
        var button = $(this);
        var text = button.text();
        button.addClass('btn-danger');
        button.removeClass('btn-warning');
        button.text('Are you sure?');
        setTimeout(
            function() {
                button.addClass('btn-warning');
                button.removeClass('btn-danger');
                button.text(text);
            }, // function
            2500
        ); // setTimeout
        e.preventDefault();
        return button;
    });
});
