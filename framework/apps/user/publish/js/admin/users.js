$(document).ready(function(){
    $('#username').keyup(function(){
        admin.g.limitInput($('#username'), 'user');
    });

    $("#username_button").on('click', function(){
        admin.users.addUser($('#username'), $('#email'), $('#password'));
    });

    $("#password_button").on('click', function(){
        user.g.generatePassword($('#password'));
    });

    $("#permission_button").on('click', function(){
        admin.users.addUserToGroup($('#add_to_user'), $('#add_to_group'), 'users_table');
    });

    $("#users_table").on('click', ".remove_user_group.btn-danger", function(){
        admin.users.removeUserFromGroup($(this));
    });
});
