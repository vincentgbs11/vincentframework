$(document).ready(function(){
    $("#update_form").on('click', "#hide_password", function(){
        user.register.toggle($("#current_password"));
    });

    $("#update_form").on('click', "#toggle_password", function(){
        user.register.toggle($("#password"));
        user.register.toggle($("#confirm"));
    });

    $("#password_button").on('click', function(){
        user.g.generatePassword($('#password'), $('#confirm'));
    });

    $("#update_button").on('click', function(){
        user.update($("#current_password"));
    });
    $("#username, #email, #new_password, #confirm").keydown(function(e){
        if (e.keyCode == 13) { // enter
            user.update($("#current_password"));
        }
    });
});
