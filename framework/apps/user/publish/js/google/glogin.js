function onSignIn(googleUser) {
        var id_token = googleUser.getAuthResponse().id_token;
        var email = googleUser.getBasicProfile().getEmail();

        $.ajax({
            url: "?url=user/user/api",
            type: 'POST',
            data: {
                login_method: 'google',
                csrf_token: $("#csrf_token").val(),
                email: email,
                id_token: id_token
            },
            success: function(response) {
                $("#google_lightbox").hide();
                console.debug(response);
            } // success
    }); // ajax
}; // onSignIn

$( document ).ready(function() {
    $("#google_login").on('click', function() {
        return user.google.getLoginButtons();
    });

    $(document).on('click', "#signOut", function() {
        return user.google.signOut();
    });

    $(document).on('click', "#close_lightbox", function() {
        return user.google.hideLightbox();
    });

    $(document).keyup(function(e) {
        if (e.keyCode == 27) { // escape
            return user.google.hideLightbox();
        }
    });
});
