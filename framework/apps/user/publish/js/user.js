var user = {
    g: { // general functions
        limitInput: function(input, limit, delay=2500) {
            {
                if (input.length <= 0) { return; }
                if (limit == 'numbers') {
                    $(input).val( $(input).val().replace(/\D/g, '') );
                } else if (limit == 'letters') {
                    $(input).val( $(input).val().replace(/[^A-Za-z]/g, '') );
                    $(input).val( $(input).val().replace(/\^/g, '') );
                } else if (limit == 'alphanumeric') {
                    $(input).val( $(input).val().replace(/[^A-Za-z0-9]/g, '') );
                    $(input).val( $(input).val().replace(/\^/g, '') );
                } else {
                    console.debug('Invalid limit variable');
                }
            }
        }, // limitInput
        flashMessage: function(message, time = 999, color = ['09F', '000']) {
            if ($("#flash_message").length <= 0) { return; }
            $("#flash_message").attr('style', 'position:absolute;z-index:9;background-color:#' + color[0] + ';color:#' + color[1]);
            $("#flash_message").html(message);
            setTimeout(
                function() {
                    $("#flash_message").attr('style', ''); // clear style
                    $("#flash_message").html(''); // clear text
                }, // function
                time
            ); // setTimeout
        }, // flashMessage
        generatePassword: function(input, match=false) {
            const password = user.g.randomNumber(10, 99) + user.g.randomSound() + user.g.randomNumber(10, 99) + user.g.randomSound() + user.g.randomNumber(10, 99) + user.g.randomSound();
            if (match) {
                match.val(password);
            }
            return input.val(password);
        },
        randomSound: function(){
            const v = 'aeiou';
            const c = 'bcdfghjkmnpqrstvwxz';
            return c[Math.floor((Math.random() * c.length))].toUpperCase() + v[Math.floor((Math.random() * v.length))] + c[Math.floor((Math.random() * c.length))];
        },
        randomNumber: function(min, max) {
            return Math.floor((Math.random() * (max-min)) + min);
        }
    }, // general functions
    register: {
        username: function(input, alert) {
            if ($(input).val().length >= 3) {
                $.ajax({
                    url: "?url=user/user/register",
                    type: "POST",
                    data: {
                        username_search: input.val()
                    },
                    success: function(response) {
                        if (response.trim() == 'Username is already taken.') {
                            $(alert).text('Username "'+input.val()+'" is already taken.');
                        } else {
                            $(alert).text('');
                        }
                    } // success
                }); // ajax
            } else {
                $(alert).text('Must be at least 3 characters');
            }
        }, // username function
        email: function(input, alert) {
            const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if (!regex.test(input.val())) {
                alert.text('Please enter a valid email address.');
            } else {
                alert.text('');
                $.ajax({
                    url: "?url=user/user/register",
                    type: "POST",
                    data: {
                        email_search: input.val()
                    },
                    success: function(response) {
                        if (response.trim() == 'Email is already taken.') {
                            alert.text('"'+input.val()+'" is already registered.');
                        } else {
                            alert.text('');
                        }
                    } // success
                }); // ajax
            }
        }, // email function
        password: function(input, alert) {
            if (input.val().length < 8) {
                alert.text('Your password must have at least 8 characters.');
            } else {
                alert.text('');
            }
        }, // password function
        confirm: function(input, confirm, alert) {
            if (confirm.val() != input.val()) {
                alert.text('Your passwords do not match.');
            } else {
                alert.text('');
            }
        }, // confirm password function
        toggle: function(input) {
            if (input.attr('type') == 'password') {
                input.attr('type', 'text');
            } else {
                input.attr('type', 'password');
            }
        }, // toggle password-text view function
        formcheck: function(selector, alert, display, verify) {
            if ($(selector).val() == '') {
                verify.verified = false;
                verify.message += display;
            } else if ($(alert).text() == '') {
                var returned = $(selector).val();
            } else {
                verify.verified = false;
                verify.message += $(alert).text() + '<br>';
            }
            return [verify, returned];
        }, // formcheck function
        submit: function(username, ualert, terms) {
            var verify = {verified: true, message: ''};
            if (username.val().length < 3) {
                verify.verified = false;
                verify.message += 'Please choose a username<br>';
            } else if (ualert.text() == '') {
                var username = username.val();
            } else {
                verify.verified = false;
                verify.message += $("#username_alert").text() + '<br>';
            }
            check = user.register.formcheck('#email', '#email_alert', 'Please enter your email address<br>', verify);
            verify = check[0];
            var email = check[1];
            check = user.register.formcheck('#password', '#password_alert', 'Please choose a password<br>', verify);
            verify = check[0];
            var password = check[1];
            check = user.register.formcheck('#confirm', '#confirm_alert', 'Please confirm your password<br>', verify);
            verify = check[0];
            if (!terms.is(':checked')) {
                verify.verified = false;
                verify.message += 'Please confirm you have read through the terms and conditions.<br>';
            }
            if (verify.verified) {
                var postData = {
                    csrf_token: $("#csrf_token").val(),
                    username: username,
                    email: email,
                    password: SHA256(password)
                }
                if ($("#referral").length && $("#referral").val() != '') {
                    postData['referral'] = $("#referral").val();
                }
                if ($("#signupcode").length && $("#signupcode").val() != '') {
                    postData['signupcode'] = $("#signupcode").val();
                }
                $.ajax({
                    url: "?url=user/user/register",
                    type: "POST",
                    data: postData,
                    success: function(response){
                        user.g.flashMessage(response, 5000);
                    }
                }); // ajax
            } else {
                user.g.flashMessage(verify.message, 2500);
            }
        }
    }, // register functions
    login: function(form, password) {
        var csrf_token = $("#csrf_token").val();
        password = password.val();
        password = $('<input>').attr({
            type: 'hidden',
            name: 'password',
            value: SHA256(password)
        })
        csrf_token = $('<input>').attr({
            type: 'hidden',
            name: 'csrf_token',
            value: csrf_token
        })
        form.append(password).append(csrf_token);
    },
    request: function(username) {
        var csrf_token = $("#csrf_token").val();
        username = username.val();
        if (username != '') {
            $.ajax({
                url: "?url=user/user/reset",
                type: "POST",
                data: {
                    csrf_token: csrf_token,
                    request: username
                },
                success: function(response){
                    user.g.flashMessage(response, 4999);
                }
            }); // ajax
        }
    }, // login functions
    resetPassword: function(reset_code, username, password, confirm) {
        var csrf_token = $("#csrf_token").val();
        var verify = true; var message = '';
        reset_code = reset_code.val();
        username = username.val();
        password = password.val();
        if (password.length < 8) {
            message += 'Your password must be at least 8 characters long.<br>';
            verify = false;
        } else {
            confirm = confirm.val();
            if (confirm == password) {
                password = SHA256(password);
            } else {
                message += 'Your passwords do not match.<br>';
                verify = false;
            }
        }
        if (verify) {
            $.ajax({
                url: "?url=user/user/reset",
                type: "POST",
                data: {
                    csrf_token: csrf_token,
                    username: username,
                    password: password,
                    reset_code: reset_code
                },
                success: function(response){
                    user.g.flashMessage(response, 4999);
                    if (response.trim() == 'Your password has been reset.') {
                        $('#login_link').removeClass('d-none');
                    }
                } //  success
            }); // ajax
        } else {
            user.g.flashMessage(message, 2499);
        }
    }, // reset password
    update: function(current_password) {
        var verify = {verified: true, message: ''};
        current_password = current_password.val();
        check = user.register.formcheck('#password', '#password_alert', 'Please enter a new password<br>', verify);
        verify = check[0];
        var password = check[1];
        check = user.register.formcheck('#confirm', '#confirm_alert', 'Please confirm your password<br>', verify);
        verify = check[0];
        var confirm = check[1];
        if (current_password == '') {
            return user.g.flashMessage('You must enter your current password to update any information.', 5000);
        } else if (verify.verified) {
            var data = {
                csrf_token: $("#csrf_token").val(),
                // username: username, email: email,
                current_password: SHA256(current_password)
            };
            if ((password == confirm) && password != '') {
                data['password'] = SHA256(password);
            // } else if (username || email) {
            } else {
                return user.g.flashMessage('Your new passwords do not match', 2500);
            }
            $.ajax({
                url: "?url=user/user/update",
                type: "POST",
                data: data,
                success: function(response) {
                    if (response.trim() == 'Profile updated.') {
                        user.g.flashMessage(response);
                    } else {
                        user.g.flashMessage(response, 9999);
                    }
                } // success
            }); // ajax
        } else {
            user.g.flashMessage(verify.message, 2500);
        }
    }, // update functions
    deactivate: function(form, password) {
        var csrf_token = $("#csrf_token").val();
        password = password.val();
        password = $('<input>').attr({
            type: 'hidden',
            name: 'password',
            value: SHA256(password)
        })
        csrf_token = $('<input>').attr({
            type: 'hidden',
            name: 'csrf_token',
            value: csrf_token
        })
        form.append(password).append(csrf_token);
    }, // deactivate functions
    google: {
        getLoginButtons: function() {
            $.ajax({
                url: "?url=user/user/api",
                type: "POST",
                data: {
                    api: 'google'
                },
                success: function(response) {
                    $("#google_lightbox").show();
                    $("#google_login_window").html(response);
                } // success
            }); // ajax
        }, // getLoginButtons
        signOut: function() {
            var auth2 = gapi.auth2.getAuthInstance();
            auth2.signOut().then(function () {
                $.ajax({
                    url: "?url=user/user/logout",
                    type: 'GET',
                    success: function(response) {
                        console.debug('User has signed out.');
                        $("#google_lightbox").hide();
                    } // success
                }); // ajax
            });
        }, // signOut
        hideLightbox: function() {
            return $("#google_lightbox").hide();
        }
    } // google login functions
};

$(document).ready(function() {
    $(document).on('click', ".btn-warning", function(e){
        let button = $(this);
        let text = button.text();
        button.addClass('btn-danger');
        button.removeClass('btn-warning');
        button.text('Are you sure?');
        setTimeout(
            function() {
                button.addClass('btn-warning');
                button.removeClass('btn-danger');
                button.text(text);
            }, // function
            2500
        ); // setTimeout
        e.preventDefault();
        return button;
    });
});
