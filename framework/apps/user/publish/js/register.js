$(document).ready(function() {
    var usernameTimer, emailTimer, passwordTimer, confirmTimer;
    $('#username').keyup(function(){
        user.g.limitInput($('#username'), 'alphanumeric');
        clearTimeout(usernameTimer);
        usernameTimer = setTimeout(function(){
            user.register.username($('#username'), $('#username_alert'));
        }, 1250);
    });
    $('#username').on('keydown', function (e) {
        if (e.keyCode != 9) { // tab
            clearTimeout(usernameTimer);
        }
    });

    $('#email').keyup(function(){
        clearTimeout(emailTimer);
        emailTimer = setTimeout(function(){
            user.register.email($('#email'), $('#email_alert'));
        }, 1000);
    });
    $('#email').on('keydown', function (e) {
        if (e.keyCode != 9) { // tab
            clearTimeout(emailTimer);
        }
    });

    $('#password').keyup(function(){
        clearTimeout(passwordTimer);
        passwordTimer = setTimeout(function(){
            user.register.password($('#password'), $('#password_alert'));
        }, 1250);
    });
    $('#password').on('keydown', function (e) {
        if (e.keyCode != 9) { // tab
            clearTimeout(passwordTimer);
        }
    });

    $('#confirm').keyup(function(){
        clearTimeout(confirmTimer);
        confirmTimer = setTimeout(function(){
            user.register.confirm($('#confirm'), $('#password'), $('#confirm_alert'));
        }, 1250);
    });
    $('#confirm').on('keydown', function (e) {
        if (e.keyCode != 9) { // tab
            clearTimeout(confirmTimer);
        }
    });

    $("#password_button").on('click', function(){
        user.g.generatePassword($('#password'), $('#confirm'));
    });

    $("#registration_form").on('click', "#toggle_password", function(){
        user.register.toggle($("#password"));
        user.register.toggle($("#confirm"));
    });

    $("#register_button").on('click', function(){
        user.register.submit($("#username"), $("#username_alert"), $('#terms'));
    });

    $("#username, #email, #password, #confirm").keydown(function(e){
        if (e.keyCode == 13) { // enter
            user.register.submit($("#username"), $("#username_alert"), $('#terms'));
        }
    });
});
