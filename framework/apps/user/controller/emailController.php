<?php
require_once FILE . 'framework/system/controller.php';

class emailController extends controller {

    public function __construct($controller='email', $app='user', $dbconnect=false)
    {
        parent::__construct('email', 'user', false);
        $this->getModel('email', 'user', $dbconnect);
        $this->getSettings('email', false, $dbconnect);
    }

    /**
     * Sends an email to a registered user's primary email, in debug mode
     * it will simply display the email to the screen. There is also a
     * display message that could tell a user to go check their inbox, etc.
     * Also takes variables that may get passed on to the mailgun API
     */
    public function email($user=false, $message=false, $title=false,
        $display=false, $mailgun=false)
    {
        if ($user && $message) {
            if (DEBUG == 'ON') {
                echo $message . '<hr>' . $display; return;
            } else if ($this->settings['EMAIL'] == 'PHP') {
                mail($user->email, $title, $message);
                echo $display; return true;
            } else if ($this->settings['EMAIL'] == 'MAILGUN'){
                $force = false; $attachment = false;
                if ($mailgun) {
                    $force = $mailgun['force'];
                    $attachment = $mailgun['attachment'];
                }
                $this->mailgun($user->email, $title, $message, $attachment, $force);
                echo $display; return true;
            } else {
                exit('System Admin: No email method selected');
            }
        } else {
            $this->log('Invalid request to "private" function');
            exit('System emailing error');
        }
    }

    /**
     * Checks for mailgun API settings, makes sure that a user is not spamming
     * the email function by checking how many emails an address has received
     * in the past 24 hours: emailModel->checkMailgun()
     */
    private function mailgun($to, $subject, $text, $attachment=false, $force=false)
    {
        $set = $this->settings; // just to make following code shorter
        if (!isset($set['mailgun_domain'], $set['mailgun_api_key'])) {
            exit('No mailgun API settings available');
        }
        $check = $this->emailModel->checkMailgun(['email_to'=>$to], $set['MAILGUNLIMIT']);
        if ($check || $force) {
            $curl_post_data = [
                'from'    => 'noreply@' . $set['mailgun_domain'],
                'to'      => $to,
                'subject' => $subject,
                'text'    => $text,
            ];
            if ($attachment) {
                $curl_post_data['attachment[1]'] = $attachment;
            }
            $url = 'https://api.mailgun.net/v3/'.$set['mailgun_domain'].'/messages';
            $curl = curl_init($url);
            curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($curl, CURLOPT_USERPWD, $set['mailgun_api_key']);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            $curl_response = curl_exec($curl);
            $response = json_decode($curl_response);
            curl_close($curl);
            if ($response && $response->id) {
                $this->emailModel->logMailgun($response->id, $to);
                echo ("To avoid spam filters, we will only send a
                    maximum of {$set['MAILGUNLIMIT']} emails a day.");
                return true;
            } else {
                $this->log($response->$message);
                exit('Error sending email.');
            }
        }
    }

}
?>
