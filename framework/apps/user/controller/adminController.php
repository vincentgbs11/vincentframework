<?php
require_once FILE . 'framework/system/controller.php';

class adminController extends controller {

    public function __construct($controller='admin', $app='user', $dbconnect=false)
    {
        parent::__construct('user', 'admin', false);
        $this->getController('user', 'user', $dbconnect);
        $this->getModel('admin', 'user', $this->userController->userModel->db);
        $this->getView('admin', 'user');
        if (!$this->userController->check()) {
            exit ('Invalid login credentials.<br><a href="?url='.HOME.'">Return to site</a>');
        }
    }

    public function home()
    {
        return $this->adminView->home();
    }

    public function permissions()
    {
        if (isset($_POST['add_permission'])) {
            $permission['function'] = strtolower($this->post('add_permission', 's', 32, '/*'));
            if ($this->adminModel->createPermission($permission['function'])) {
                $permission['permission_id'] = $this->adminModel->last_insert_id();
                echo json_encode($permission);
                return true;
            } else {
                exit('Error creating permission');
            }
        } else if (isset($_POST['delete_permission'])) {
            $permission = $this->post('delete_permission', 'i');
            if ($this->adminModel->deletePermission($permission)) {
                if ($this->adminModel->deletePermissionInGroup($permission)) {
                    exit('Deleted');
                } else {
                    exit('Error deleting permission from groups');
                }
            } else {
                exit('Error deleting permission');
            }
        } else if (isset($_POST['search'])) {
            $permissions = $this->adminModel->selectPermissions();
            echo json_encode($permissions);
            return true;
        } else {
            return $this->adminView->permissions($this->adminModel->selectPermissions());
        }
    }

    public function groups()
    {
        if (isset($_POST['add_group'])) {
            $group['group_name'] = strtolower($this->post('add_group', 'l', 99));
            if ($this->adminModel->createGroup($group['group_name'])) {
                $group['group_id'] = $this->adminModel->last_insert_id();
                echo json_encode($group);
                return true;
            } else {
                exit('Error creating group');
            }
        } else if (isset($_POST['remove_group'])) {
            $group = $this->post('remove_group', 'i', 11);
            if ($this->adminModel->deleteGroup($group)) {
                if ($this->adminModel->deleteGroupAndPermissions($group)) {
                    if ($this->adminModel->deleteGroupAndUsers($group)) {
                        exit('Deleted');
                    } else {
                        exit('Error deleting group users');
                    }
                } else {
                    exit('Error deleting group permissions');
                }
            } else {
                exit('Error deleting group');
            }
        } else if (isset($_POST['add_permission'], $_POST['group'])) {
            $group['group_id'] = $this->post('group', 'i', 11);
            $group['permission_ids'] = $this->post('add_permission', 's', 99, ',');
            $group['permissions'] = explode(',', $group['permission_ids']);
            foreach($group['permissions'] as $permission) {
                $check = $this->adminModel->createPermissionInGroup($group['group_id'], $permission);
                if (!$check) {
                    exit('Error adding permission to group');
                }
            }
            $permissions = $this->adminModel->selectGroups(false, $group['group_id']);
            echo json_encode($permissions);
            return true;
        } else if (isset($_POST['remove_permission'], $_POST['group'])) {
            $group = $this->post('group', '1', 11);
            $permission = $this->post('remove_permission', 'i', 11);
            if ($this->adminModel->deletePermissionInGroup($permission, $group)) {
                exit('Permission deleted from group');
            } else {
                exit('Error removing permission from group');
            }
        } else {
            $groups['permissions'] = $this->adminModel->selectPermissions();
            $groups['group_list'] = $this->adminModel->getGroups();
            $groups['groups'] = $this->adminModel->selectGroups();
            return $this->adminView->groups($groups);
        }
    }

    public function users()
    {
        if (isset($_POST['add_user'], $_POST['email'], $_POST['password'])) {
            $user = ['other_signup_info' => NULL];
            $user['username'] = strtolower($this->post('add_user', 'a', 99));
            $user['email'] = strtolower($this->post('email', 'e', 255));
            $user['password'] = $this->post('password', 'a', 64);
            $user = new userObject($user);
            if ($this->userController->registerUser($user)) {
                $user->user_id = $this->adminModel->last_insert_id();
                if ($this->userController->userModel->activateUser($user)) {
                    if ($this->userController->userModel->createPermissions($user, [$this->userController->settings['DEFAULTGROUP']])) {
                        exit($user->username . ' added');
                    } else {
                        exit('Error adding user to default group');
                    }
                } else {
                    exit('Error activating user');
                }
            } else {
                exit('Error adding user');
            }
        } else if (isset($_POST['add_group'], $_POST['user'])) {
            $user = $this->post('user', 'i', 11);
            $group = $this->post('add_group', 'i', 11);
            if ($this->adminModel->createUserInGroup($user, $group)) {
                $user = $this->adminModel->selectUsers(['ulu`.`user_id'=>$user, 'urg`.`group_id'=>$group]);
                echo json_encode($user[0]);
                return true;
            } else {
                exit('Error adding user to group');
            }
        } else if (isset($_POST['remove_group'], $_POST['user'])) {
            $user = $this->post('user', 'i', 11);
            $group = $this->post('remove_group', 'i', 11);
            if ($this->adminModel->deleteUserInGroup($user, $group)) {
                exit('User removed from group');
            } else {
                exit('Error removing using from group');
            }
        } else {
            $users['user_list'] = $this->adminModel->getUsers();
            $users['group_list'] = $this->adminModel->getGroups();
            $users['users'] = $this->adminModel->selectUsers();
            return $this->adminView->users($users);
        }
    }

    public function settings()
    {
        return $this->adminView->settings($this->userController->settings);
    }
}
?>
