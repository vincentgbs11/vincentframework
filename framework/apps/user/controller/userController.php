<?php
require_once FILE . 'framework/system/controller.php';
require_once FILE . 'framework/system/php/session.php';

class userController extends controller {

    public function __construct($controller='user', $app='user', $dbconnect=false)
    {
        parent::__construct('user', 'user', $dbconnect);
        $this->getModel('user', 'user', $dbconnect);
        $this->getView('user');
        $this->getObject('user');
        $this->getSettings('user');
        $this->session = new SESSION();
        $this->session->checkSession();
    }

    public function home()
    {
        if ($this->check()) {
            return $this->userView->home($_SESSION['USER']);
        } else {
            return $this->redirect('user/user/login');
        }
    }

    public function register($url=false)
    {
        /**
         * Handles registration for users. userController handles input
         * validation passes input to userObject and registers that user,
         * then emails an activation link to that user.
         */
        if (isset($_POST['username'], $_POST['email'], $_POST['password'])) {
            // csrfCheck($url=false, $exit=true); defaults to URL
            if ($this->csrfCheck()) {
                if (isset($_POST['referral'])) {
                    $other['referral'] = $this->post('referral', 'a', 99);
                }
                if (isset($_POST['signupcode'])) {
                    $other['signupcode'] = $this->post('signupcode', 'a', 99);
                }
                $user = new userObject();
                $user->username = strtolower($this->post('username', 'a', 99));
                $user->email = strtolower($this->post('email', 'e', 255));
                $user->password = $this->post('password', 'a', 64);
                $register = $this->registerUser($user);
                if ($register) {
                    if (isset($other)) {
                        $this->userModel->otherUserInfo($register['user'], $other);
                    }
                    $this->getController('email', 'user', $this->userModel->db);
                    $this->emailController->email(
                        $register['user'], $register['message'],
                        'Activation Link', $register['display']);
                }
                return true;
            } // csrfCheck exit if no match
        /* Checks if a username is already taken; Mainly for register page */
        } else if (isset($_POST['username_search'])) {
            if ($this->userModel->checkUsername(
                $this->post('username_search', 'a', 99))) {
                exit('Username is already taken.');
            } else {
                exit('Username is still available.');
            }
        /* Checks if a email is already taken; Mainly for register page */
        }  else if (isset($_POST['email_search'])) {
            if ($this->userModel->checkEmail(
                $this->post('email_search', 'e', 255))) {
                exit('Email is already taken.');
            } else {
                exit('Email is still available.');
            }
        }
        /* Adds referral and signup codes that display on page when added to GET */
        $other['signupcode'] = $this->get('signupcode', 'a', 99);
        $other['referral'] = $this->get('referral', 'a', 99);
        return $this->userView->register($other);
    }

    public function terms()
    {
        return $this->userView->terms();
    }

    public function activate()
    {
        $user = new userObject();
        $user->activation = $this->get('activation', 'a', 40);
        $user = $this->userModel->checkActivationCode($user);
         /* Activates user if the activation code exists for any user */
        if ($user) {
            $this->userModel->activateUser($user);
            $this->userModel->createPermissions($user, [$this->settings['DEFAULTGROUP']]);
        }
        return $this->redirect('user/user/login');
    }

    public function registerUser($user=NULL, $activate=true)
    {
        if (isset($user)) {
            if (strlen($user->username) < 3) {
                exit($user->username . ' is not a valid username.');
            }
            if ($this->userModel->checkUsername($user->username)) {
                exit($user->username . ' is already taken.');
            }
            if (!filter_var($user->email, FILTER_VALIDATE_EMAIL)) {
                exit($user->email . ' is not a valid email address.');
            } else if ($this->userModel->checkEmail($user->email)) {
                exit($user->email . ' is already registered.');
            }
            $user->createActivationCode($this->userModel);
            $user->hashPassword($user->password);
            if ($this->userModel->createUser($user) && $activate) {
                $user->user_id = $this->userModel->last_insert_id();
                return $user->activationEmail();
            }
        }
        $this->log('Invalid request to "private" function');
    }

    /* redirects to a given url */
    public function redirect($url=NULL, $goToLastPage=false, $setLastPage=false)
    {
        if (isset($url)) {
            if ($setLastPage) { $_SESSION['last_page'] = $setLastPage; }
            if ($goToLastPage && isset($_SESSION['last_page'])) {
                $url = $_SESSION['last_page'];
                unset($_SESSION['last_page']);
            }
            if (URLTYPE == 'PRETTY') {
                header('Location: /' . $url);
            } else {
                header('Location: ?url=' . $url);
            }
        }
    }

    public function login()
    {
        if ($this->check()) {
            return $this->redirect('user/user/home');
        } else if (isset($_POST['username'], $_POST['password'])) {
            if ($this->csrfCheck()) {
                $email = $this->post('username', 'e', 255);
                $user = $this->checkEmailOrUsername($email, ['active'=>1]);
                if ($user === NULL) {
                    echo ('The email, username or password you entered was invalid.<br>
                    Repeated failed attempts may result in your account being locked out.');
                    return $this->userView->login();
                }
                $brute = $this->userModel->readBrute($user, $this->settings['BRUTETIME']);
                if (count($brute) >= $this->settings['MAXATTEMPTS']) {
                    echo ("You have attempted too many login attempts
                    ({$this->settings['MAXATTEMPTS']}). Please try again later.");
                    return $this->userView->login();
                }
                $password = $this->post('password', 'a', 64);
                $user->ip();
                if ($user->checkPassword($password)) {
                    $this->setSession($user);
                    $this->userModel->createLogin($user, 1);
                    return $this->redirect(HOME, true);
                } else {
                    $this->userModel->createLogin($user);
                    echo ('The email, username or password you entered was invalid.<br>
                    Repeated failed attempts may result in your account being locked out.');
                    return $this->userView->login();
                }
            } // csrfCheck
        }
        return $this->userView->login();
    }

    private function checkEmailOrUsername($check, $user=[])
    {
        if (stristr($check, '@')) {
            $user['email'] = $check;
        }
        else {
            $user['username'] = $check;
        }
        $user = new userObject($user);
        return $this->checkUserExists($user);
    }

    /* Flexible function allows check of user by different model functions */
    private function checkUserExists($user, $query='readUser')
    {
        $user = $this->userModel->$query($user);
        if (isset($user[0], $user[0]->user_id)) {
            $user = new userObject($user[0]);
            return $user;
        } // else return NULL
    }

    private function setSession($user)
    {
        $_SESSION['USER'] = $user;
        return $this->session->refreshSession();
    }

    public function logout()
    {
        $this->session->endSession();
        return $this->redirect('user/user/login');
    }

    /**
     * Commentary: Not loving the multi use of userObject and checkUserExists
     * in this function, but it works and avoids extra lines of code.
     */
    public function reset()
    {
        if (isset($_POST['username'], $_POST['password'], $_POST['reset_code'])) {
            if ($this->csrfCheck()) {
                $user = new userObject(['reset_code'=> $this->post('reset_code', 'a', 64)]);
                $user = $this->checkUserExists($user, 'readReset');
                if ($user === NULL) {
                    exit('Your reset code has expired.');
                } else {
                    $check = $this->checkEmailOrUsername(
                        $this->post('username', 'a', 255), ['active'=>1]);
                    if ($user->user_id == $check->user_id) {
                        if ($this->resetPassword($check, $this->post('password', 'a', 64))) {
                            return $this->userModel->deleteReset($user);
                        } else {
                            $this->log('Unknown error resetting user password');
                            exit('Database error resetting password');
                        }
                    } else {
                        exit('Your username or email does not match this reset code.');
                    }
                }
            } // csrfCheck
        } else if (isset($_POST['request'])) {
            $user = new userObject();
            if ($this->csrfCheck()) {
                $email = $this->post('request', 'e', 255);
                $user = $this->checkEmailOrUsername($email, ['active'=>1]);
                if ($user === NULL) {
                    exit('Username or email not found');
                } // else
                $reset = $user->createResetCode($this->userModel);
                if ($reset) {
                    $this->getController('email', 'user', $this->userModel->db);
                    $this->emailController->email($reset['user'], $reset['message'], 'Reset Link', $reset['display']);
                }
                return true;
            } // csrfCheck
        }
        if (isset($_GET['reset_code'])) {
            $user = new userObject(['reset_code'=> $this->get('reset_code', 'a', 64)]);
            if (!$this->checkUserExists($user, 'readReset')) {
                echo ('Invalid reset code: Your code may have expired.');
                return $this->userView->reset(NULL);
            }
            return $this->userView->reset($user);
        }
        return $this->userView->request();
    }

    private function resetPassword($user, $password)
    {
        $user = (object)['user_id' => $user->user_id];
        $reset = new userObject();
        $reset->hashPassword($password);
        if ($this->userModel->updateUser($user, $reset)) {
            $this->userModel->deleteBrute($user);
            echo ('Your password has been reset.');
            return true;
        }
    }

    public function deactivate()
    {
        if ($this->check()) {
            if (isset($_POST['password'])) {
                if ($this->csrfCheck()) {
                    if ($_SESSION['USER']->checkPassword($this->post('password', 'a', 64))) {
                        $this->userModel->deactivateUser($_SESSION['USER']);
                        $this->session->endSession();
                        echo('User deactivated');
                        return $this->userView->register();
                    } else {
                        exit('Incorrect password.');
                    }
                } // csrfCheck
            }
            return $this->userView->deactivate($_SESSION['USER']);
        } else {
            return $this->redirect('user/user/login', false, URL);
        }
    }

    /**
     * Most common userController function called from other classes
     * Checks if a user is logged in and checks if they have valid permissions
     */
    public function check()
    {
        if (isset($_SESSION['USER'])) {
            if ($this->checkPermissions($_SESSION['USER'])) {
                return true;
            } else {
                echo ('401 Authorization Error: A system administrator has been notified of your request');
                return $this->userView->home($_SESSION['USER']);
            }
        } else {
            return false;
        }
    }

    private function checkPermissions($user)
    {
        $user->getPermissions($this->userModel);
        $universal = explode('/', URL)[0] . '/' . explode('/', URL)[1] .'/*';
        if (in_array($universal, $user->permissions)) {
            return true;
        } else if (in_array(URL, $user->permissions)) {
            return true;
        } else {
            $this->userModel->createAccess($user);
            return false;
        }
    }

    public function update()
    {
        if ($this->check()) {
            if (isset($_POST['current_password'])) {
                if ($this->csrfCheck()) {
                    $password = $this->post('current_password', 'a', 64);
                    if ($_SESSION['USER']->checkPassword($password)) {
                        if (isset($_POST['password'])) {
                            $update = new userObject();
                            $update->hashPassword($this->post('password', 'a', 64));
                            $user = new userObject(['user_id' => $_SESSION['USER']->user_id]);
                            $this->userModel->updateUser($user, $update);
                            exit('Profile updated.');
                        }
                    } else {
                        exit('Incorrect password.');
                    }
                } // csrfCheck
            } else {
                return $this->userView->update($_SESSION['USER']);
            }
        } else {
            return $this->redirect('user/user/login', false, URL);
        }
    }

    public function api()
    {
        if (isset($_POST['login_method'])) {
            $login = $this->post('login_method', 'a', 255);
            switch ($login) {
                case 'google':
                    if ($this->csrfCheck()) {
                        $token = $this->post('id_token', NULL);
                        $email = $this->post('email', 'e', 255);
                        return $this->googleLogin($token, $email);
                    } // csrfCheck
                break;
                default: exit('Invalid login request.');
            }
        } else if (isset($_POST['api'])) {
            $api = $this->post('api', 'a', 255);
            switch ($api) {
                case 'google':
                    if (isset($this->settings['google_client_id'], $this->settings['DEFAULTGROUP'])) {
                        return $this->userView->google($this->settings);
                    } else {
                        exit('Missing google settings.');
                    }
                break;
                default: exit('Invalid login request.');
            }
        } else if (isset($_GET['login_api'])) {
            $login = $this->get('login_api', 'a', 255);
            switch ($login) {
                case 'google':
                    return $this->userView->google();
                break;
                default: exit('Invalid login method.');
            }
        }
    }

    private function googleLogin($token, $email)
    {
        $url = 'https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=' . $token;
        $output = json_decode($this->fetch($url));
        if ($output === null) {
            exit('Google Authentification Error');
        }
        if ($output->aud == $this->settings['google_client_id']
            && $output->email == $email) {
            if ($output->exp <= 0) {
                exit('Google Authentification has expired');
            }
            $user = $this->checkUserExists(['email'=>$email]);
            if ($user) {
                $this->setSession($user);
                exit('Logged in.');
            } else {
                $user = new userObject(['email'=>$email]);
                $user->username = 'google_' .
                    preg_replace("/[^a-zA-Z0-9]/", '',
                        str_replace('@gmail.com', '', $email)
                    );
                $user->password = 'registered_with_google';
                $user->salt = 'registered_with_google';
                $user->activation = 'registered_with_google';
                $register = $this->registerUser($user);
                if ($register) {
                   $user = $this->checkUserExists(['email'=>$email]); // get new user_id
                   $this->userModel->createPermissions($user, [$this->settings['DEFAULTGROUP']]);
                   $this->setSession($user);
                   exit("Logged in as {$email}.");
               } else {
                   exit('Error registering gmail');
              }
            }
        } else { exit('Google Authentification Error'); }
    }

}
?>
