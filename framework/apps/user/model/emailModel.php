<?php
require_once FILE . 'framework/system/model.php';

class emailModel extends model {

    /* Logs when an email has been sent from the mailgun API */
    public function logMailgun($id, $email)
    {
        $q = "INSERT INTO `mailgun_ls_emails` (`mailgun_id`, `email_to`)
            VALUES ({$this->wrap($id)}, {$this->wrap($email)});";
        return $this->execute($q);
    }

    /**
     * Checks that an email is not spamming requests, default limit
     * is 5 emails per day. This can be overwritten with the $force
     * variable in emailController->mailgun(... $force)
     */
    public function checkMailgun($params, $lim, $interval='1 DAY')
    {
        $q = 'SELECT * FROM `mailgun_ls_emails` WHERE ';
        foreach ($params as $key => $value) {
            $q .= " `{$key}`={$this->wrap($value)} AND";
        }
        $q .= " `timestamp` >= now() - INTERVAL {$interval} LIMIT {$lim};";
        $check = $this->select($q);
        if (count($check) >= $lim) {
            exit("To avoid spam filters, we only send a maximum of {$lim}
                emails a day. Please try again tomorrow.");
        } else {
            return true;
        }
    }

}
?>
