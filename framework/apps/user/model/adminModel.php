<?php
require_once FILE . 'framework/system/model.php';

class adminModel extends model {

    public function selectPermissions($like=false)
    {
        $q = 'SELECT `permission_id`, `function` FROM `user_ls_permissions`';
        if ($like) {
            $q .= " WHERE `function` LIKE '{$like}%'";
        }
        $q .= ';';
        return $this->select($q);
    }

    public function createPermission($permission)
    {
        $q = "INSERT INTO `user_ls_permissions` (`function`)
            VALUES ({$this->wrap($permission)});";
        return $this->execute($q);
    }

    public function deletePermission($permission)
    {
        $q = "DELETE FROM `user_ls_permissions`
            WHERE permission_id={$this->wrap($permission)}";
        return $this->execute($q);
    }

    public function createPermissionInGroup($group, $permission)
    {
        $q = "INSERT INTO `user_rel_permissions` (`group_id`, `permission_id`)
            VALUES ({$group}, {$permission});";
        return $this->execute($q);
    }

    public function deletePermissionInGroup($permission, $group=false)
    {
        $q = "DELETE FROM `user_rel_permissions`
            WHERE `permission_id`={$permission}";
            if ($group) {
                $q .= " AND `group_id`={$group}";
            }
            $q .= ';';
        return $this->execute($q);
    }

    public function getGroups()
    {
        $q = 'SELECT `group_id`, `group_name` FROM `user_ls_groups`;';
        return $this->select($q);
    }

    public function selectGroups($like=false, $id=false)
    {
        $q = 'SELECT `urp`.`group_id`, `group_name`, `urp`.`permission_id`, `function`
            FROM `user_rel_permissions` AS `urp`
            LEFT JOIN `user_ls_groups` AS `rlg` ON `urp`.`group_id`=`rlg`.`group_id`
            LEFT JOIN `user_ls_permissions` AS `ulp` ON `urp`.`permission_id`=`ulp`.`permission_id`';
        if ($like) {
            $q .= " WHERE `group_name` LIKE '{$like}%'";
        } else if ($id) {
            $q .= " WHERE `urp`.`group_id` = {$id}";
        }
        $q .= ';';
        return $this->select($q);
    }

    public function createGroup($group)
    {
        $q = "INSERT INTO `user_ls_groups` (`group_name`)
            VALUES ({$this->wrap($group)});";
        return $this->execute($q);
    }

    public function deleteGroup($group)
    {
        $q = "DELETE FROM `user_ls_groups`
            WHERE `group_id`={$group};";
        return $this->execute($q);
    }

    public function deleteGroupAndPermissions($group)
    {
        $q = "DELETE FROM `user_rel_permissions`
            WHERE `group_id`={$group};";
        return $this->execute($q);
    }

    public function deleteGroupAndUsers($group)
    {
        $q = "DELETE FROM `user_rel_groups`
            WHERE `group_id`={$group};";
        return $this->execute($q);
    }

    public function getUsers($params=false, $lim=99)
    {
        $q = 'SELECT `user_id`, `username`, `email`, `registration_date`, `active`
            FROM `user_ls_users`';
        if ($params) {
            $q .= ' WHERE';
            foreach ($params as $key => $value) {
                $q .= " `{$key}`={$this->wrap($value)} AND";
            }
            $q = substr($q, 0, -4);
        }
        $q .= " LIMIT {$lim};";
        return $this->select($q);
    }

    public function selectUsers($users=false, $groups=false)
    {
        $q = 'SELECT `ulu`.`user_id`, `username`, `email`, `registration_date`,
            `urg`.`group_id`, `group_name`
            FROM `user_ls_users` AS `ulu`
            LEFT JOIN `user_rel_groups` AS `urg` ON `ulu`.`user_id`=`urg`.`user_id`
            LEFT JOIN `user_ls_groups` AS `ulg` ON `urg`.`group_id`=`ulg`.`group_id`';
        if ($users) {
            $q .= ' WHERE';
            foreach ($users as $key => $value) {
                $q .= " `{$key}`={$this->wrap($value)} AND";
            }
            $q = substr($q, 0, -4);
        } elseif ($groups) {
            $q .= " WHERE `group_name` LIKE '{$groups}%'";
        }
        $q .= ';';
        return $this->select($q);
    }

    public function createUserInGroup($user, $group)
    {
        $q = "INSERT INTO `user_rel_groups` (`user_id`, `group_id`)
            VALUES ({$user}, {$group});";
        return $this->execute($q);
    }

    public function deleteUserInGroup($user, $group=false)
    {
        $q = "DELETE FROM `user_rel_groups`
            WHERE `user_id`={$user}";
            if ($group) {
                $q .= " AND `group_id`={$group}";
            }
            $q .= ';';
        return $this->execute($q);
    }
}
?>
