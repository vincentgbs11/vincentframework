<?php
class userObject {

    public $user_id = NULL;
    public $username = NULL;
    public $email = NULL;
    public $password = NULL;
    public $salt = NULL;
    public $activation = NULL;
    public $active = NULL;
    public $registration_date = NULL;

    public function __construct($array=[])
    {
        foreach ($array as $key => $value) {
            $this->$key = $value;
        }
    }

    public function createActivationCode($userModel)
    {
        do {$this->activation = hash('sha1', mt_rand());
        } while ($userModel->createActivationCode($this->activation));
    }

    public function hashPassword($password, $salt=NULL)
    {
        if (ENCRYPTION === 'BCRYPT') {
            $this->password = password_hash($password, PASSWORD_BCRYPT, ['cost'=>12]);
            return $this->salt = 'BCRYPT_COMBINES_SALT_WITH_PASSWORD';
        } else if (ENCRYPTION === 'PBKDF2') {
            if ($salt === NULL) { // create a salt if it doesn't already exist
                $salt = bin2hex(random_bytes(32));
            }
            foreach (range(1, HASHITERATIONS) as $i) {
                $password = hash('sha256', $salt . $password);
            }
            $this->password = $password;
            $this->salt = $salt;
            return true;
        } else {
            exit('System Admin: Missing password configuration.');
        }
    }

    /* returns an array with parameters to send an email */
    public function activationEmail()
    {
        $message = 'Please confirm your email with this link:
            <a href="{{{$url}}}?url=user/user/activate&activation={{{$activation}}}">Activate</a>.';
        if (DEBUG == 'ON') {
            $message = str_replace('{{{$url}}}', DOMAIN, $message);
        } else {
            $message = str_replace('{{{$url}}}', DISPLAYDOMAIN, $message);
        }
        $message = str_replace('{{{$activation}}}', $this->activation, $message);
        return ['user'=>$this, 'message'=>$message,
        'display'=>'Please check your email for the activation link.'];
    }

    public function checkPassword($password)
    {
        if (ENCRYPTION === 'BCRYPT') {
            return password_verify($password, $this->password);
        } else if (ENCRYPTION === 'PBKDF2') {
            $check = new userObject();
            $check->hashPassword($password, $this->salt);
            return ($check->password === $this->password);
        }
    }

    public function ip()
    {
        $ip['REMOTE_ADDR'] = (isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : NULL);
        (isset($_SERVER['HTTP_CLIENT_IP'])?$ip['HTTP_CLIENT_IP'] = $_SERVER['HTTP_CLIENT_IP']:NULL);
        (isset($_SERVER['HTTP_X_FORWARDED_FOR'])?$ip['HTTP_X_FORWARDED_FOR'] = $_SERVER['HTTP_X_FORWARDED_FOR']:NULL);
        (isset($_SERVER['HTTP_X_FORWARDED'])?$ip['HTTP_X_FORWARDED'] = $_SERVER['HTTP_X_FORWARDED']:NULL);
        (isset($_SERVER['HTTP_X_CLUSTER_CLIENT_IP'])?$ip['HTTP_X_CLUSTER_CLIENT_IP'] = $_SERVER['HTTP_X_CLUSTER_CLIENT_IP']:NULL);
        (isset($_SERVER['HTTP_FORWARDED_FOR'])?$ip['HTTP_FORWARDED_FOR'] = $_SERVER['HTTP_FORWARDED_FOR']:NULL);
        (isset($_SERVER['HTTP_FORWARDED'])?$ip['HTTP_FORWARDED'] = $_SERVER['HTTP_FORWARDED']:NULL);
        $this->ip = serialize($ip);
    }

    public function createResetCode($userModel)
    {
        do {$this->reset_code = bin2hex(random_bytes(32));
        } while ( $userModel->checkResetCode($this->reset_code) );
        $userModel->createReset($this);
        return $this->resetEmail();
    }

    /* returns an array with parameters to send an email */
    public function resetEmail()
    {
        $message = 'You can reset your password by going to:
            <a href="{{{$url}}}?url=user/user/reset&reset_code={{{$reset}}}">Reset Password</a>';
        if (DEBUG == 'ON') {
            $message = str_replace('{{{$url}}}', DOMAIN, $message);
        } else {
            $message = str_replace('{{{$url}}}', DISPLAYDOMAIN, $message);
        }
        $message = str_replace('{{{$reset}}}', $this->reset_code, $message);
        return ['user'=>$this, 'message'=>$message, 'display'=>'Please check your email for the reset link.'];
    }

    public function getPermissions($userModel)
    {
        $this->permissions = [];
        $permissions = $userModel->readPermissions($this);
        if (is_array($permissions)) {
            foreach ($permissions as $permission) {
                $this->permissions[] = $permission->function;
            }
        }
    }

}

?>
