<?php $this->js('reset'); ?>
<div class="row col-12" id="reset_form">
    <h3>Password reset form</h3>
    <div class="row col-12 form-group">
        <label for="username">Username or email</label>
        <input type="text" class="form-control" id="username" aria-describedby="usernameHelp" maxlength="255">
        <small id="usernameHelp" class="form-text text-muted">
            Please confirm your username or email
        </small>
    </div> <!-- </div class="row col-12 form-group"> -->
    <div class="row col-12 form-group">
        <label for="password">New password</label>
        <input type="text" class="form-control" id="password" aria-describedby="passwordHelp" maxlength="99" autofocus>
        <small id="passwordHelp" class="form-text text-muted">
            <li>At least 8 characters long</li>
            <li><button class="btn btn-outline-dark btn-sm" id="password_button">Generate password</button></li>
            <li>
                <input type="checkbox" class="form-check-input" id="toggle_password">
                <label for="toggle_password">Hide passwords</label>
            </li>
        </small>
    </div> <!-- </div class="row col-12 form-group"> -->
    <div class="row col-12 form-group">
        <label for="confirm">Confirm new password</label>
        <input type="text" class="form-control" id="confirm" maxlength="99">
    </div> <!-- </div class="row col-12 form-group"> -->
    <div class="row">
        <input type="hidden" id="reset_code" value="<?php echo (isset($data->reset_code)?$data->reset_code:NULL); ?>">
        <button class="btn btn-outline-dark" id="reset_button" <?php echo (isset($data->reset_code)?NULL:'disabled'); ?>>Reset Password</button>
        &#160;<a href="<?php $this->url('user/user/login'); ?>"><button class="btn btn-outline-dark d-none" id="login_link">Login</button></a>
    </div><!-- </div class="row"> -->
</div>
