<?php $this->js('register'); ?>
<div class="row col-12" id="registration_form">
    <h3>Registration form</h3>
    <div class="row col-12 form-group">
        <label for="username">Username</label>
        <input type="text" class="form-control" id="username" aria-describedby="usernameHelp" maxlength="99" required>
        <small id="usernameHelp" class="form-text text-muted">
            <li>At least 3 characters long</li>
            <li>Alphanumeric characters only</li>
        </small>
        <p class="alert" id="username_alert"></p>
    </div> <!-- </div class="row col-12 form-group"> -->
    <div class="row col-12 form-group">
        <label for="email">Email</label>
        <input type="email" class="form-control" id="email" aria-describedby="emailHelp" maxlength="255" required>
        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
        <p class="alert" id="email_alert"></p>
    </div> <!-- </div class="row col-12 form-group"> -->
    <div class="row col-12 form-group">
        <label for="password">Password</label>
        <input type="text" class="form-control" id="password" aria-describedby="passwordHelp" maxlength="99" required>
        <small id="passwordHelp" class="form-text text-muted">
            <li>At least 8 characters long</li>
            <li><button class="btn btn-outline-dark btn-sm" id="password_button">Generate password</button></li>
            <li>
                <input type="checkbox" class="form-check-input" id="toggle_password">
                <label for="toggle_password">Hide passwords</label>
            </li>
        </small>
        <p class="alert" id="password_alert"></p>
    </div> <!-- </div class="row col-12 form-group"> -->
    <div class="row col-12 form-group">
        <label for="confirm">Confirm password</label>
        <input type="text" class="form-control" id="confirm" maxlength="99" required>
        <p class="alert" id="confirm_alert"></p>
    </div> <!-- </div class="row col-12 form-group"> -->
    <?php if ($data['referral'] != '') { ?>
        <div class="row col-12 form-group">
            <label for="confirm">Referred by</label>
            <input type="text" class="form-control" id="referral" value="<?php echo $data['referral']; ?>" placeholder="username" maxlength="99"/>
        </div> <!-- </div class="row col-12 form-group"> -->
    <?php } ?>
    <?php if ($data['signupcode'] != '') { ?>
        <div class="row col-12 form-group">
            <label for="confirm">Sign up code</label>
            <input type="text" class="form-control" id="signupcode" value="<?php echo $data['signupcode']; ?>" maxlength="99"/>
        </div> <!-- </div class="row col-12 form-group"> -->
    <?php } ?>
    <div class="row col-12 form-check">
        <input type="checkbox" class="form-check-input" id="terms" required>
        <label class="form-check-label" for="terms">I agree to the <a href="?url=user/user/terms>" target="_blank">Terms and Conditions</a>.</label>
    </div><!-- </div class="row col-12 form-check"> -->
    <div class="row col-12 text-center">
        <div class="offset-md-5">
            <button class="btn btn-outline-dark" id="register_button">Register</button>
        </div> <!-- </div class="col-4"> -->
    </div><!-- </div class="row"> -->
</div>
