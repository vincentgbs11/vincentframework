<?php $this->js('google/glogin'); ?>
<?php $this->css('google'); ?>
<div class="row col-12">
    <h3>Google Login</h3>
    <p class="container">Register and sign in using your google account. We only ask for your: <strong>name, email address, and profile picture</strong>. These are only used for account verification purposes. We will not ask for access to any other information regarding your google account.</p>
    <div class="row col-12">
        <button class="btn btn-outline-dark" id="google_login">Login with Google</button>
    </div>
    <div id="google_lightbox"><div id="google_login_window"></div></div>
</div>
