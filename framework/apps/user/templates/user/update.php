<?php $this->js('register'); ?>
<?php $this->js('update'); ?>
<div class="row col-12" id="update_form">
    <h3>Update password</h3>
    <div class="row col-12 form-group">
        <label for="password">Current password</label>
        <input type="text" class="form-control" id="current_password" maxlength="99" autofocus required>
        <small id="PasswordHelp" class="form-text text-muted">
            <li>
                <input type="checkbox" class="form-check-input" id="hide_password">
                <label for="hide_password">Hide password</label>
            </li>
        </small>
    </div> <!-- </div class="row col-12 form-group"> -->
    <div class="row col-12 form-group">
        <label for="username">Username</label>
        <input type="text" class="form-control" id="username" aria-describedby="usernameHelp" maxlength="99" value="<?php echo $data->username?>" disabled>
        <!-- <small id="usernameHelp" class="form-text text-muted"> -->
            <!-- <li>At least 3 characters long</li> -->
            <!-- <li>Alphanumeric characters only</li> -->
        <!-- </small> -->
        <!-- <p class="alert" id="username_alert"></p> -->
    </div> <!-- </div class="row col-12 form-group"> -->

    <div class="row col-12 form-group">
        <label for="email">Email</label>
        <input type="email" class="form-control" id="email" aria-describedby="emailHelp" maxlength="255" value="<?php echo $data->email?>" disabled>
        <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
        <!-- <p class="alert" id="email_alert"></p> -->
    </div> <!-- </div class="row col-12 form-group"> -->

    <div class="row col-12 form-group">
        <label for="password">New password</label>
        <input type="text" class="form-control" id="password" aria-describedby="PasswordHelp" maxlength="99">
        <small id="PasswordHelp" class="form-text text-muted">
            <li>At least 8 characters long</li>
            <li><button class="btn btn-outline-dark btn-sm" id="password_button">Generate password</button></li>
            <li>
                <input type="checkbox" class="form-check-input" id="toggle_password">
                <label for="toggle_password">Hide passwords</label>
            </li>
        </small>
        <p class="alert" id="password_alert"></p>
    </div> <!-- </div class="row col-12 form-group"> -->
    <div class="row col-12 form-group">
        <label for="confirm">Confirm new password</label>
        <input type="text" class="form-control" id="confirm" maxlength="99">
        <p class="alert" id="confirm_alert"></p>
    </div> <!-- </div class="row col-12 form-group"> -->

    <div class="row col-12 text-center">
        <div class="offset-md-5">
            <button class="btn btn-outline-dark" id="update_button">Update</button>
        </div> <!-- </div class="col-4"> -->
    </div><!-- </div class="row col-12 text-center"> -->
</div>
