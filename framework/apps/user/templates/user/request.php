<?php $this->js('request'); ?>
<div class="row col-md-12">
    <div class="row col-12" id="request_form">
        <h3>Request reset link</h3>
        <div class="row col-12 form-group">
            <label for="username">Username or email</label>
            <input type="text" class="form-control" id="username" aria-describedby="usernameHelp" maxlength="255" autofocus>
            <small id="usernameHelp" class="form-text text-muted">
                Please note that the reset link will only be valid for 15 minutes.
                    After that time you will need to request a new link.
            </small>
            <p class="alert" id="username_alert"></p>
        </div> <!-- </div class="row col-12 form-group"> -->
        <div class="row col-12 text-center">
            <div class="offset-md-5">
                <button class="btn btn-outline-dark" id="request_button">Request</button>
            </div> <!-- </div class="col-4"> -->
        </div><!-- </div class="row"> -->
    </div><!-- </div class="row col-12" id="request_form"> -->
</div>
