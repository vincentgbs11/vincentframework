<?php $this->js('deactivate'); ?>
<form method="POST" action="<?php $this->url('user/user/deactivate'); ?>" id="deactivate_form">
    <h3>Deactivate account</h3>
    <div class="row col-12 form-group">
        <label for="username">Username</label>
        <input type="text" class="form-control" id="username" name="username" value="<?php echo $data->username; ?>" disabled>
        <small class="form-text text-muted">
            If you deactivate your account, you will need to contact a system administrator to reactivate it.
        </small>
    </div> <!-- </div class="row col-12 form-group"> -->
    <div class="row col-12 form-group">
        <label for="password">Password</label>
        <input type="text" class="form-control" id="password" aria-describedby="passwordHelp" maxlength="99" autofocus required>
        <small id="passwordHelp" class="form-text text-muted">
            <input type="checkbox" id="toggle_password">
            <label for="toggle_password">Hide password</label>
        </small>
        <p class="alert" id="password_alert"></p>
    </div> <!-- </div class="row col-12 form-group"> -->
    <div class="row col-12 text-center">
        <div class="offset-md-5">
            <button class="btn btn-warning" id="deactivate_button">Deactivate</button>
        </div> <!-- </div class="col-4"> -->
    </div><!-- </div class="row"> -->
</form>
