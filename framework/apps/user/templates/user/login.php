<?php $this->js('login'); ?>
<form method="POST" action="<?php $this->url('user/user/login'); ?>" id="login_form">
    <h3>Login</h3>
    <div class="row col-12 form-group">
        <label for="username">Email or username</label>
        <input type="text" class="form-control" id="username" name="username" maxlength="255" autofocus required>
        <p class="alert" id="username_alert"></p>
    </div> <!-- </div class="row col-12 form-group"> -->
    <div class="row col-12 form-group">
        <label for="password">Password</label>
        <input type="text" class="form-control" id="password" aria-describedby="passwordHelp" maxlength="99" required>
        <small id="passwordHelp" class="form-text text-muted">
            <input type="checkbox" id="toggle_password">
            <label for="toggle_password">Hide password</label>
        </small>
        <p class="alert" id="password_alert"></p>
    </div> <!-- </div class="row col-12 form-group"> -->
    <div class="row col-12 text-center">
        <div class="offset-md-5">
            <button class="btn btn-outline-dark" id="login_button">Login</button>
        </div> <!-- </div class="col-4"> -->
    </div><!-- </div class="row"> -->
</form>
