<div class="row col-md-11">
    <h2>Terms and Conditions ("Terms")</h2>
    <hr/>
    <p>Last updated: (March 14, 2019)</p>
    <p>Please read these Terms and Conditions ("Terms", "Terms and Conditions") carefully before using our website or mobile application (the "Service").
    Your access to and use of the Service is conditioned upon your acceptance of and compliance with these Terms. These Terms apply to all visitors, users, and others who access or use the Service.
    By accessing or using the Service you agree to be bound by these Terms. If you disagree with any part of the terms then you may not access the Service.</p>
    <h3>Purchases</h3>
    <p>If you wish to purchase any product or service made available through the Service ("Purchase"), you may be asked to supply certain information relevant to completing your Purchase.</p>
    <h3>Content</h3>
    <p>Our Service allows you to post, share and otherwise make available certain information, text, graphics, videos, or other material ("Content"). You, the user, are responsible for the legality of this content. All gameplay is offered on an "as is" and "as available" basis, without warranties of any kind, either express or implied, including, without limitations, warrenties of merchantability or fitness for a particular purpose.</p>
    <h3>Links To Other Web Sites</h3>
    <p>Our Service may contain links to third-party web sites or services that are not owned or controlled by us.
    We have no control over, and assumes no responsibility for, the content, privacy policies, or practices of any third party web sites or services. You further acknowledge and agree that we shall not be responsible or liable, directly or indirectly, for any damage or loss caused or alleged to be caused by or in connection with use of or reliance on any such content, goods or services available on or through any such web sites or services.</p>
    <h3>Changes</h3>
    <p>We reserve the right, at our sole discretion, to modify or replace these Terms at any time. If a revision is material we will try to provide at least 30 days' notice prior to any new terms taking effect. What constitutes a material change will be determined at our sole discretion.</p>
    <h3>Contact Us</h3>
    <p>If you have any questions about these Terms and Conditions, please contact us using the following form, or contacting us as our customer service email: <a href="mailto:contact@<?php echo DISPLAYDOMAIN; ?>">contact@<?php echo DISPLAYDOMAIN; ?></a>. Thank you.
        <div id="contact">
            <form>
                <input type="text" placeholder="name">
                <input type="text" placeholder="email">
                <input type="text" placeholder="message">
                <button class="btn btn-outline-dark" disabled>Contact</button>
            </form>
        </div>
    </p>
</div>
