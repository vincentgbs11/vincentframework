<table class="table" id="settings_table">
    <caption>View only</caption>
    <thead class="table-active">
        <tr>
            <td>setting_name</td>
            <td>setting_value</td>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($data AS $name => $value) {
        echo "<tr><td>{$name}</td><td>{$value}</td></tr>";
    } ?>
    </tbody>
</table>
