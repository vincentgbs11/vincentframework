<?php $this->js('admin/groups'); ?>
<div class="row col-12" id="groups_form">
    <div class="row col-12 form-group">
        <label for="group">Add group</label>
        <input type="text" class="form-control" id="group" aria-describedby="groupHelp" maxlength="99">
        <button class="btn btn-outline-dark" id="group_button">Add group</button>
        <small id="groupHelp" class="form-text text-muted">
            &#160;New group name (max 99 characters)
        </small>
    </div> <!-- </div class="row col-12 form-group"> -->
    <div class="col-12">
        <div class="col-12 form-group">
            <label for="add_to_permission">Add these permissions...</label>
            <select multiple class="form-control" id="add_to_permission">
                <?php foreach ($data['permissions'] AS $permission) {
                    echo "<option permission='$permission->permission_id'>{$permission->function}</option>";
                } ?>
            </select>
        </div><!-- </div class="col-12 form-group"> -->
        <div class="col-12 form-group">
            <label for="add_to_group">... to this group</label>
            <select class="form-control" id="add_to_group">
                <?php foreach ($data['group_list'] AS $group) {
                    echo "<option group='$group->group_id'>{$group->group_name}</option>";
                } ?>
            </select>
            <button class="btn btn-outline-dark" id="permission_button">Add permission to group</button>
            <button class="btn btn-warning float-right" id="delete_group">Delete group</button>
        </div><!-- </div class="col-12 form-group"> -->
    </div> <!-- </div class="col-12"> -->
</div> <!-- </div class="row col-12" id="groups_form"> -->
<table class="table" id="groups_table">
    <caption>The delete button in each row will remove a permission from a group. The group delete button will delete a group and remove all permissions and users attached to that group.</caption>
    <thead class="table-active">
        <tr>
            <td>group_id</td>
            <td>group_name</td>
            <td>permission_id</td>
            <td>function</td>
            <td>delete</td>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($data['groups'] AS $group) {
            echo "<tr><td>{$group->group_id}</td>
                <td>{$group->group_name}</td>
                <td>{$group->permission_id}</td>
                <td>{$group->function}</td>
                <td><button group='{$group->group_id}' permission='{$group->permission_id}'
                    class='remove_permission btn btn-warning'>delete</button></td></tr>";
        } ?>
    </tbody>
</table>
