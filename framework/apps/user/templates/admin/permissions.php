<?php $this->js('admin/permissions'); ?>
<div class="row col-12" id="permissions_form">
    <div class="row col-12 form-group">
        <label for="permission">Add permission</label>
        <input type="text" class="form-control" id="permission" aria-describedby="permissionHelp" maxlength="32">
        <button class="btn btn-outline-dark" id="permission_button">Add permission</button>
        <small id="permissionHelp" class="form-text text-muted">
            &#160;Permission formats: "xxxxx/xxxxx/xxxxx" or "xxxxx/xxxxx/*"
        </small>
    </div> <!-- </div class="row col-12 form-group"> -->
</div> <!-- </div class="row col-12" id="permissions_form"> -->
<table class="table" id="permissions_table">
    <caption>A list of all website permissions</caption>
    <thead class="table-active">
        <tr>
            <td>permission_id</td>
            <td>function</td>
            <td>delete</td>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($data AS $permission) {
            echo "<tr><td>{$permission->permission_id}</td>
                <td>{$permission->function}</td>
                <td><button permission='{$permission->permission_id}'
                    class='delete_permission btn btn-warning'>delete</button></td></tr>";
        } ?>
    </tbody>
</table>
