<?php $this->js('admin/users'); ?>
<div class="row col-12" id="users_form">
    <div class="row col-12 form-group">
        <label for="username">Add user</label>
        <input type="text" class="form-control" id="username" aria-describedby="usernameHelp" maxlength="99" placeholder="username">
        <input type="text" class="form-control" id="email" maxlength="255" placeholder="email">
        <input type="text" class="form-control" id="password" maxlength="99" placeholder="password">
        <div class="row col-12">
            <div class="row col-6">
                <button class="btn btn-outline-dark" id="username_button">Add user</button>
                <small id="usernameHelp" class="form-text text-muted">
                    &#160;New username (max 99 characters)
                </small>
            </div><!-- </div class="row col-6"> -->
            <div class="row col-6">
                <small id="passwordHelp" class="form-text text-muted">
                    <li>New Password (at least 8 characters long)</li>
                    <li>
                        <input type="checkbox" class="form-check-input" id="toggle_password">
                        <label for="toggle_password">Hide passwords</label>
                    </li>
                </small>
                <button class="btn btn-outline-dark" id="password_button">Generate password</button>
            </div><!-- </div class="row col-6"> -->
        </div> <!-- </div class="row col-12"> -->
    </div> <!-- </div class="row col-12 form-group"> -->
    <div class="col-12">
        <div class="col-12 form-group">
            <label for="add_to_user">Add user...</label>
            <select class="form-control" id="add_to_user">
                <?php foreach ($data['user_list'] AS $user) {
                    echo "<option user='$user->user_id'>{$user->username}</option>";
                } ?>
            </select>
        </div><!-- </div class="col-12 form-group"> -->
        <div class="col-12 form-group">
            <label for="add_to_group">... to this group</label>
            <select class="form-control" id="add_to_group">
                <?php foreach ($data['group_list'] AS $group) {
                    echo "<option group='$group->group_id'>{$group->group_name}</option>";
                } ?>
            </select>
            <button class="btn btn-outline-dark" id="permission_button">Add user to group</button>
        </div><!-- </div class="col-12 form-group"> -->
    </div> <!-- </div class="col-12"> -->
</div> <!-- </div class="row col-12" id="users_form"> -->
<table class="table" id="users_table">
    <caption>The delete button in each row will remove a user from a group.</caption>
    <thead class="table-active">
        <tr>
            <td>user_id</td>
            <td>username</td>
            <td>email</td>
            <td>registration_date</td>
            <td>group_id</td>
            <td>group_name</td>
            <td>delete</td>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($data['users'] AS $users) {
            echo "<tr><td>{$users->user_id}</td>
                <td>{$users->username}</td>
                <td>{$users->email}</td>
                <td>{$users->registration_date}</td>
                <td>{$users->group_id}</td>
                <td>{$users->group_name}</td>
                <td><button user='{$users->user_id}' group='{$users->group_id}'
                    class='remove_user_group btn btn-warning'>delete</button></td></tr>";
        } ?>
    </tbody>
</table>
