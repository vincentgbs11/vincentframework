<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="author" content="">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>User Management Module</title>

    <link rel="stylesheet" type="text/css" href="/css/library/4.1.3.bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/css/library/1.12.1.jquery-ui.min.css">
    <script src="/js/library/jquery-3.2.1.min.js"
            integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
            crossorigin="anonymous"></script>
    <script src="/js/library/1.12.1.jquery-ui.min.js"
            type="text/javascript"></script>
    <script src="/js/library/sha256.js" type="text/javascript"></script>
    <script src="/js/vanilla.js" type="text/javascript"></script>
    <?php $this->js('user'); ?>
    <?php $this->js('admin/admin'); ?>
</head>

<body class="container">
    <div id="noscript_lightbox">
        <div class="text-center bg-primary lead">Javascript not loaded. Your browser may not support JavaScript.</div>
    </div>
    <div id="flash_message"></div>

    <div class="row col-12 nav_bar">
        <a href="/"><button class="btn btn-outline-dark">Home</button></a>
        <a href="<?php $this->url('user/user/update') ?>"><button class="btn btn-outline-dark">Update Account</button></a>
        <a href="<?php $this->url('user/user/register') ?>"><button class="btn btn-outline-dark">Register</button></a>
        <a href="<?php $this->url('user/user/login') ?>"><button class="btn btn-outline-dark">Login</button></a>
        <a href="<?php $this->url('user/user/logout') ?>"><button class="btn btn-outline-dark">Logout</button></a>
        <a href="<?php $this->url('user/user/reset') ?>"><button class="btn btn-outline-dark">Reset Password</button></a>
        <a href="<?php $this->url('user/user/deactivate') ?>"><button class="btn btn-outline-dark">Deactivate</button></a>
    </div> <!-- </div class="row col-12"> -->
    <div class="row col-12 nav_bar">
        <a href="<?php $this->url('user/admin/permissions') ?>"><button class="btn btn-outline-secondary">Permissions</button></a>
        <a href="<?php $this->url('user/admin/groups') ?>"><button class="btn btn-outline-secondary">Groups</button></a>
        <a href="<?php $this->url('user/admin/users') ?>"><button class="btn btn-outline-secondary">Users</button></a>
        <a href="<?php $this->url('user/admin/settings') ?>"><button class="btn btn-outline-secondary">Settings</button></a>
    </div> <!-- </div class="row col-12"> -->
