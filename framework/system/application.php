<?php
abstract class application
{
    /**
     * Requires controller name, assumes controller is in the same app
     * takes $this->someModel->db as the dbconnect to avoid creating extra mysql
     * connections whenever possible. attaches 'some' as $this->someController
     */
    public function getController(string $controller, $app=false, $dbconnect=false)
    {
        if ($app === false) {
            $app = $this->app;
        }
        $newController = $controller . 'Controller';
        include_once FILE . "framework/apps/{$app}/controller/{$newController}.php";
        $this->$newController = new $newController($controller, $app, $dbconnect);
    }

    /**
     * Requires model name, assumes model is in the same app
     * takes $this->someModel->db as the dbconnect to avoid creating extra
     * mysql connections whenever possible. attaches 'some' as $this->someModel
     */
    public function getModel(string $model, $app=false, $dbconnect=false)
    {
        if ($app === false) {
            $app = $this->app;
        }
        $newModel = $model . 'Model';
        include_once FILE . "framework/apps/{$app}/model/{$newModel}.php";
        $this->$newModel = new $newModel($model, $app, $dbconnect);
    }

    /**
     * Requires view name, assumes view is in the same app
     * attaches 'some' as $this->someView
     */
    public function getView(string $view, $app=false)
    {
        if ($app === false) {
            $app = $this->app;
        }
        $view .= 'View';
        include_once FILE . "framework/apps/{$app}/view/{$view}.php";
        $this->$view = new $view($app);
    }

    /**
     * Requires object name, assumes object is in the same app
     * if return is specified, returns the object, otherwise just
     * includes the class file to instantiate the object
     */
    public function getObject(string $object, $app=false, $return=false)
    {
        if ($app === false) {
            $app = $this->app;
        }
        $object .= 'Object';
        include_once FILE . "framework/apps/{$app}/objects/{$object}.php";
        if ($return) {
            $object = new $object();
            return $object;
        }
    }

    /**
     * Allows any model, view, or controller to create logs;
     * logs the file name and line, along with a given message
     */
    protected function log(string $message)
    {
        $fp = fopen(FILE . 'framework/log.txt', 'a');
        $header = '(' . URL . '): ';
        fwrite($fp, $header . $message . PHP_EOL);
        fclose($fp);
    }

}
?>
