<?php
require_once FILE . 'framework/system/application.php';

abstract class view extends application {

    /**
     * Adds header, body and footer for appending $html; Requires that templates
     * used in a view come from a single app ($this->file use in loadTemplate).
     */
    public function __construct($app)
    {
        $this->app = $app;
        $this->file = FILE . "framework/apps/{$app}/templates/";
        $this->header = '';
        $this->body = '';
        $this->footer = '';
    }

    /**
     * Loads a template file to someView's header, body or footer. Can pass
     * data from controller. Assumes loading to body, if loaded to footer,
     * it will be appended before the current footer, otherwise appended after.
     * $misc is for rarely used loading function parameters
     */
    public function loadTemplate($template, $data=null, $load='body', $misc=false)
    {
        ob_start();
        require($this->file . $template . '.php');
        $html = ob_get_contents();
        ob_end_clean();
        if ($misc && isset($misc['return'])
        && ($misc['return'] == true)) {
            return $html;
        }
        if ($load == 'footer') {
            $this->$load = $html . $this->$load;
        }
        if ($misc && isset($misc['reload'])
        && ($misc['reload'] == true)) {
            $this->$load = $html;
        } else {
            $this->$load .= $html;
        }
    }

    /* Displays html from view, create CSRF tokens at display time */
    public function display($all=true, $csrf=true)
    {
        if ($all) {
            echo $this->header;
        }
        if ($csrf) {
            $_SESSION['CSRF_TOKENS'][URL] = bin2hex(random_bytes(32));
            $this->body .= '<input type="hidden" id="csrf_token" value="'.$_SESSION['CSRF_TOKENS'][URL].'"/>';
        }
        echo $this->body;
        if ($all) {
            echo $this->footer;
        }
    }

    /**
     * Links css to page, can be called from template files. Uses internal css
     * as well as published links (minified) to display on page.
     */
    public function css($css, $app=false)
    {
        if ($app === false) { $app = $this->app; }
        if (file_exists(FILE . "html/css/apps/{$app}/{$css}.css")) {
            echo '<link rel="stylesheet" type="text/css" href="'.DOMAIN.'/css/apps/'.$app.'/'.$css.'.css">';
        } else {
            ob_start();
            require(FILE . "framework/apps/{$app}/publish/css/{$css}.css");
            $css = ob_get_contents();
            ob_end_clean();
            echo '<style>' . $css . '</style>';
        }
    }

    /**
     * Links js to page, can be called from template files. Uses internal (raw)
     * js as well as published links (obfuscated, minified) to display on page.
     * Published js should load faster on production as it allows caching.
     */
    public function js($js, $app=false)
    {
        if ($app === false) { $app = $this->app; }
        if (file_exists(FILE . "html/css/apps/{$app}/{$js}.js")) {
            echo '<script type="text/javascript" src="'.DOMAIN.'/js/apps/'.$app.'/'.$js.'.js"></script>';
        } else {
            ob_start();
            require(FILE . "framework/apps/{$app}/publish/js/{$js}.js");
            $js = ob_get_contents();
            ob_end_clean();
            echo '<script>' . $js . '</script>';
        }
    }

    /* Displays urls in set format, can be called from template files */
    public function url($local)
    {
        if (URLTYPE == 'PRETTY') {
            echo "/{$local}";
        } else { // URLTYPE == 'GET'
            echo "?url={$local}";
        }
    }

}
?>
