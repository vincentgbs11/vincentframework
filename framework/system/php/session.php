<?php
class SESSION
{

    /**
     * Checks the status of the session. Checks if HTTPS is required from
     * config.php. Checks if a session has expired, or if there are signs of
     * session hijacking (different browser or wrong session id). If the
     * session looks fine, refreshes it as needed or starts a new one if
     * none was there before.
     */
    public function checkSession($name='USER_SESSION_ID', $path='/')
    {
        if (session_id() == '') {
            session_start();
            if (isset($_SESSION['CREATED'])) {
                if (HTTPS === 1 && !isset($_COOKIE['USER_SESSION_ID'])) {
                    exit('<h1>You are not using a secure (https) connection</h1>');
                }
                if (time() > $_SESSION['EXPIRE'] || // session has expired
                    $_SESSION['HTTP_USER_AGENT'] != $_SERVER['HTTP_USER_AGENT'] || // different browser
                    $_SESSION['USER_SESSION_ID'] != $_COOKIE['USER_SESSION_ID']) { // wrong session id
                    return $this->endSession();
                } else if (time() - $_SESSION['CREATED'] > 60) {
                    $this->refreshSession();
                }
            } else { // start a new session
                return $this->startSession($name);
            }
        }
    }

    /**
     * Start a new custom session with framework set expiration time and
     * semi-random custom session_id. Sets that cookie to the user's brower.
     * 'USER_SESSION_ID' is separate from 'PHPSESSID' which is the standard
     * php session cookie sent. Both are used to track framework sessions
     */
    private function startSession($name)
    {
        $_SESSION['CREATED'] = time();
        $_SESSION['EXPIRE'] = time() + (LOGINEXPIRE * 86400);
        $_SESSION['HTTP_USER_AGENT'] = $_SERVER['HTTP_USER_AGENT'];
        $_SESSION['USER_SESSION_ID'] = bin2hex(random_bytes(32)); // random
        $domain = str_replace('https://', '', str_replace('http://', '', DOMAIN));
        return $this->setCookie($name, $_SESSION['USER_SESSION_ID']);
    }

    public function refreshSession()
    {
        session_regenerate_id(true);
        $_SESSION['CREATED'] = time();
    }

    /**
     * Empties SESSION on the server side; sets empty PHPSESSID cookie and
     * empty USER_SESSION_ID cookies, then clears SESSION on the server
     */
    public function endSession($name='USER_SESSION_ID')
    {
        $_SESSION = [];
        setcookie('PHPSESSID', '', 0, '/', '', HTTPS, 1);
        $this->setCookie($name, '', 0);
        return session_destroy();
    }

    /**
     * Checks https settings and sets cookie if all seems good.
     * setcookie($name, $value, $expires, $path, $domain, $secure, $httponly)
     * secure: 1 only use HTTPS, 0 allow regular HTTP
     * httponly: 1 accessible only through http protocol
     * 0 cookie is accessible through js
     */
    public function setCookie($name, $value='', $time=NULL, $https=NULL, $js=1)
    {
        if ($time === NULL) { $time = LOGINEXPIRE * 60 * 60 * 24; }
        if ($https === NULL) { $https = HTTPS; }
        if (HTTPS === 1 && (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'on')) {
            return false;
        } // else (HTTPS is not required || HTTP isset && 'on')
        return setcookie($name, $value, (time() + $time), '/', '', $https, $js);
    }

}
?>
