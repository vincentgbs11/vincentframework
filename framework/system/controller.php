<?php
require_once FILE . 'framework/system/application.php';

abstract class controller extends application {

    public function __construct($controller, $app, $dbconnect=false)
    {
        $this->app = $app;
        $this->controller = $controller;
    }

    /* Default home class, for index.php */
    public function home() { echo 'Hello World'; }

    /* Cleans the $_GET global variable */
    protected function get($var, $type=null, $len=null, $special=false)
    {
        if (!isset($_GET[$var])) { return false; }
        if ($type == 'i') { // integer
            $text = filter_input(INPUT_GET, $var, FILTER_SANITIZE_NUMBER_INT);
            } elseif ($type == 'f') { // float
                $text = filter_input(INPUT_GET, $var, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
            } elseif ($type == 'l') { // letters
                $text = filter_input(INPUT_GET, $var, FILTER_SANITIZE_STRING);
                $text = preg_replace("/[^a-zA-Z]/", '', $text);
            } elseif ($type == 'a') { // alphanumeric
                $text = filter_input(INPUT_GET, $var, FILTER_SANITIZE_STRING);
                $text = preg_replace("/[^a-zA-Z0-9]/", '', $text);
            } elseif ($type == 'u') { // alphanumeric and underscore
                $text = filter_input(INPUT_GET, $var, FILTER_SANITIZE_STRING);
                $text = preg_replace("/[^a-zA-Z0-9_]/", '', $text);
            } elseif ($type == 's' && $special) { // alphanumeric and 'special characters'
                $text = filter_input(INPUT_GET, $var, FILTER_SANITIZE_SPECIAL_CHARS);
                if (stristr($special, '/')) { $special = str_replace('/', '\/', $special); }
                $text = preg_replace("/[^a-zA-Z0-9{$special}]/", '', $text);
            } elseif ($type == 'e') { // emails
                $text = filter_input(INPUT_GET, $var, FILTER_SANITIZE_EMAIL);
            } elseif ($type == 'w') { // (words) alphanumeric and spaces
                $text = filter_input(INPUT_GET, $var, FILTER_SANITIZE_STRING);
                $text = preg_replace("/[^a-zA-Z0-9\ ]/", '', $text);
            } else {
                $text = filter_input(INPUT_GET, $var, FILTER_SANITIZE_SPECIAL_CHARS);
            }
        if (isset($len) && $len < strlen($text)) {
            $text = substr($text, 0, $len);
        }
        return htmlspecialchars($text);
    }

    /* Cleans the $_POST global variable */
    protected function post($var, $type=null, $len=null, $special=false)
    {
        if (!isset($_POST[$var])) { return false; }
        if ($type == 'i') { // integer
            $text = filter_input(INPUT_POST, $var, FILTER_SANITIZE_NUMBER_INT);
            } elseif ($type == 'f') { // float
                $text = filter_input(INPUT_POST, $var, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
            } elseif ($type == 'l') { // letters
                $text = filter_input(INPUT_POST, $var, FILTER_SANITIZE_STRING);
                $text = preg_replace("/[^a-zA-Z]/", '', $text);
            } elseif ($type == 'a') { // alphanumeric
                $text = filter_input(INPUT_POST, $var, FILTER_SANITIZE_STRING);
                $text = preg_replace("/[^a-zA-Z0-9]/", '', $text);
            } elseif ($type == 'u') { // alphanumeric and underscore
                $text = filter_input(INPUT_POST, $var, FILTER_SANITIZE_STRING);
                $text = preg_replace("/[^a-zA-Z0-9_]/", '', $text);
            } elseif ($type == 's' && $special) { // alphanumeric and 'special characters'
                $text = filter_input(INPUT_POST, $var, FILTER_SANITIZE_SPECIAL_CHARS);
                if (stristr($special, '/')) { $special = str_replace('/', '\/', $special); }
                $text = preg_replace("/[^a-zA-Z0-9{$special}]/", '', $text);
            } elseif ($type == 'e') { // emails
                $text = filter_input(INPUT_POST, $var, FILTER_SANITIZE_EMAIL);
            } elseif ($type == 'w') { // (words) alphanumeric and spaces
                $text = filter_input(INPUT_POST, $var, FILTER_SANITIZE_STRING);
                $text = preg_replace("/[^a-zA-Z0-9\ ]/", '', $text);
            } else {
                $text = filter_input(INPUT_POST, $var, FILTER_SANITIZE_SPECIAL_CHARS);
            }
        if (isset($len) && $len < strlen($text)) {
            $text = substr($text, 0, $len);
        }
        return htmlspecialchars($text);
    }

    /* Checks for CSRF token match from $this->someView->display() */
    protected function csrfCheck($url=false, $exit=true)
    {
        $csrf = filter_input(INPUT_POST, 'csrf_token', FILTER_SANITIZE_STRING);
        $csrf = substr(preg_replace("/[^a-zA-Z0-9]/", '', $csrf), 0, 64);
        if (isset($_SESSION['CSRF_TOKENS'])) {
            return ($csrf === $_SESSION['CSRF_TOKENS'][($url ? $url : URL)]);
        } elseif ($exit) {
            exit('Csrf token mismatch. Please refresh your page and try again.');
        } // else return NULL;
    }

    /* A simple backend curl function */
    public function fetch($url, $post=false)
    {
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        if ($post) {
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($post));
        }
        $html = curl_exec($curl);
        curl_close($curl);
        return $html;
    }

    /* Takes raw data from mysql and prepares it for display in html */
    protected function database_decode($content)
    {
        $content = html_entity_decode(htmlspecialchars_decode($content));
        $content = preg_replace("/(&#39;)/", "'", $content);
        return $content;
    }

    /**
     * Gets settings from the `sys_ls_settings` table and attaches it to the
     * controller as an array in $this->settings[]; Assumes the model is the
     * same as category; dbconnect to avoid creating extra mysql connection
     */
    protected function getSettings($category, $model=false, $dbconnect=false)
    {
        if ($model) {
            $model = $model . 'Model';
        } else {
            $model = $category . 'Model';
        }
        if (isset($this->$model)) {
            $model = $this->$model;
        } else {
            $this->getModel($category, $this->app, $dbconnect);
            $model = $this->$model;
        }
        $this->settings = [];
        $settings = $model->getSettings($category);
        foreach ($settings as $setting) {
            $this->settings[$setting->setting_name] = $setting->setting_value;
        }
    }

}
?>
