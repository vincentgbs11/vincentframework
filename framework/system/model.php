<?php
require_once FILE . 'framework/system/application.php';

abstract class model extends application {

    public function __construct($model, $app, $dbconnect=false)
    {
        if (!function_exists('mysqli_init') && !extension_loaded('mysqli')) {
            exit('500 Internal Server Error: Database connection error');
        }
        $this->app = $app;
        $this->model = $model;
        if ($dbconnect && ($dbconnect->connect_error === NULL)) {
            $this->db = $dbconnect;
        } else {
            $this->db = new mysqli(DBHOST, DBUSER, DBPASS, DATABASE);
        }
        if ($this->db->connect_errno) {
            exit('500 Internal Server Error: Database connection error');
        }
    }

    /**
     * Simple select query: returns an array of objects from a select statement
     * each object in the array is a line from the select statement.
     */
    public function select($query)
    {
        $result = $this->db->query($query, MYSQLI_USE_RESULT);
        $return = [];
        while ($row = $result->fetch_object()) {
            $return[] = $row;
        }
        $result->close();
        return $return;
    }

    /* Simple execute query: can be used for insert, update or delete */
    public function execute($query)
    {
        return $this->db->query($query);
    }

    /* Wraps text in single quotes, escapes ; and ' to avoid SQL injection */
    public function wrap($text, $slash=";'")
    {
        if (isset($text)) {
            return "'" . addcslashes($text, $slash) . "'";
        } else {
            return 'NULL';
        }
    }

    public function last_insert_id()
    {
        $last_insert_id = $this->select('SELECT LAST_INSERT_ID() AS `id`;')[0]->id;
        return $last_insert_id;
    }

    /**
     * Gets settings from the `sys_ls_settings` table to return to controller
     */
    public function getSettings($category)
    {
        $q = "SELECT `setting_name`, `setting_value`
            FROM `sys_ls_settings` WHERE `cat`='{$category}';";
        return $this->select($q);
    }

}
?>
