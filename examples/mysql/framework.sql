# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.24-0ubuntu0.16.04.1)
# Database: php_framework
# Generation Time: 2018-10-10 10:10:10 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

# Creating the php_framework database
# ------------------------------------------------------------
DROP DATABASE IF EXISTS php_framework;
-- create and select the php_framework database
CREATE DATABASE `php_framework` DEFAULT CHARACTER SET = `utf8` DEFAULT COLLATE = `utf8_unicode_ci`;
USE php_framework;

# Creating the php_framework user
# ------------------------------------------------------------
DROP USER IF EXISTS 'php_framework'@'localhost';
-- create a user for the framework
CREATE USER 'php_framework'@'localhost' IDENTIFIED BY 'password';
GRANT ALL PRIVILEGES ON `php\_framework`.* TO 'php_framework'@'localhost';
FLUSH PRIVILEGES;

# Dump of table mailgun_ls_emails
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mailgun_ls_emails`;

CREATE TABLE `mailgun_ls_emails` (
  `mailgun_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_to` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  UNIQUE KEY `mailgun_id` (`mailgun_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table short_ls_shortcuts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `short_ls_shortcuts`;

CREATE TABLE `short_ls_shortcuts` (
  `shortcut_id` int(11) NOT NULL AUTO_INCREMENT,
  `shortcut` varchar(99) COLLATE utf8_unicode_ci DEFAULT NULL,
  `destination` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`shortcut_id`),
  UNIQUE KEY `shortcut` (`shortcut`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `short_ls_shortcuts` WRITE;
/*!40000 ALTER TABLE `short_ls_shortcuts` DISABLE KEYS */;

INSERT INTO `short_ls_shortcuts` (`shortcut_id`, `shortcut`, `destination`)
VALUES
	(1,'admin','user/admin/home'),
	(2,'user','user/user/login'),
	(3,'logout','user/user/logout'),
	(4,'register','user/user/register'),
	(5,'short','short/short/admin');

/*!40000 ALTER TABLE `short_ls_shortcuts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sys_ls_settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sys_ls_settings`;

CREATE TABLE `sys_ls_settings` (
  `setting_id` int(11) NOT NULL,
  `setting_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `setting_value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cat` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `memo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`setting_id`),
  UNIQUE KEY `setting_name` (`setting_name`,`cat`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `sys_ls_settings` WRITE;
/*!40000 ALTER TABLE `sys_ls_settings` DISABLE KEYS */;

INSERT INTO `sys_ls_settings` (`setting_id`, `setting_name`, `setting_value`, `cat`, `memo`)
VALUES
	(1,'DEFAULTGROUP','1','user',NULL),
	(2,'MAXATTEMPTS','9','user','maximum login attemps in BRUTETIME'),
	(3,'BRUTETIME','1 HOUR','user','time before user can try more login attempts'),
    (11,'EMAILER','PHP','email','MAILGUN, PHP'),
	(12,'MAILGUNLIMIT','5','email','max number of emails per day, per email'),
	(13,'mailgun_api_key','api:key-1234567890abc1234567890','email',''),
	(14,'mailgun_domain','sandbox1234567890abc1234567890.mailgun.org','email','');


/*!40000 ALTER TABLE `sys_ls_settings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_ls_access
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_ls_access`;

CREATE TABLE `user_ls_access` (
  `user_id` int(11) NOT NULL,
  `url` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  UNIQUE KEY `user_id` (`user_id`,`url`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table user_ls_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_ls_groups`;

CREATE TABLE `user_ls_groups` (
  `group_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `group_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`group_id`),
  UNIQUE KEY `group_name` (`group_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `user_ls_groups` WRITE;
/*!40000 ALTER TABLE `user_ls_groups` DISABLE KEYS */;

INSERT INTO `user_ls_groups` (`group_id`, `group_name`)
VALUES
	(2,'admin'),
	(1,'user');

/*!40000 ALTER TABLE `user_ls_groups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_ls_login
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_ls_login`;

CREATE TABLE `user_ls_login` (
  `user_id` int(11) unsigned NOT NULL,
  `ip_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `success` int(1) unsigned NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table user_ls_other
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_ls_other`;

CREATE TABLE `user_ls_other` (
  `user_id` int(11) unsigned NOT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table user_ls_permissions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_ls_permissions`;

CREATE TABLE `user_ls_permissions` (
  `permission_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `function` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`permission_id`),
  UNIQUE KEY `function` (`function`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `user_ls_permissions` WRITE;
/*!40000 ALTER TABLE `user_ls_permissions` DISABLE KEYS */;

INSERT INTO `user_ls_permissions` (`permission_id`, `function`)
VALUES
	(3,'short/short/admin'),
	(2,'user/admin/*'),
	(1,'user/user/*');

/*!40000 ALTER TABLE `user_ls_permissions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_ls_reset
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_ls_reset`;

CREATE TABLE `user_ls_reset` (
  `user_id` int(11) unsigned NOT NULL,
  `reset_code` varchar(64) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  UNIQUE KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table user_ls_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_ls_users`;

CREATE TABLE `user_ls_users` (
  `user_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(99) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `salt` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `activation` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `active` int(1) unsigned NOT NULL,
  `registration_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `activation` (`activation`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `user_ls_users` WRITE;
/*!40000 ALTER TABLE `user_ls_users` DISABLE KEYS */;

INSERT INTO `user_ls_users` (`user_id`, `username`, `email`, `password`, `salt`, `activation`, `active`, `registration_date`)
VALUES
	(1,'admin','admin@vincentframework.localhost','7c53e83b55861bf3e6e5aa527e22b060c299a7052c4e0192baeb62756915d577','5b7af8a8ef8f5989cb807ce84af853a3add12a728967eeff6ce2dbfb4cee0fae','784ff4ff3490531a5e3789436bd85463158231d1',1,'2018-01-01 12:30:30');

/*!40000 ALTER TABLE `user_ls_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_rel_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_rel_groups`;

CREATE TABLE `user_rel_groups` (
  `user_id` int(11) unsigned NOT NULL,
  `group_id` int(11) unsigned NOT NULL,
  UNIQUE KEY `user_id` (`user_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `user_rel_groups` WRITE;
/*!40000 ALTER TABLE `user_rel_groups` DISABLE KEYS */;

INSERT INTO `user_rel_groups` (`user_id`, `group_id`)
VALUES
	(1,1),
	(1,2);

/*!40000 ALTER TABLE `user_rel_groups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_rel_permissions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_rel_permissions`;

CREATE TABLE `user_rel_permissions` (
  `group_id` int(11) unsigned NOT NULL,
  `permission_id` int(11) unsigned NOT NULL,
  UNIQUE KEY `group_id` (`group_id`,`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `user_rel_permissions` WRITE;
/*!40000 ALTER TABLE `user_rel_permissions` DISABLE KEYS */;

INSERT INTO `user_rel_permissions` (`group_id`, `permission_id`)
VALUES
	(1,1),
	(2,2),
	(2,3);

/*!40000 ALTER TABLE `user_rel_permissions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_view_permissions
# ------------------------------------------------------------

DROP VIEW IF EXISTS `user_view_permissions`;

CREATE TABLE `user_view_permissions` (
   `user_id` INT(11) UNSIGNED NULL DEFAULT '0',
   `email` VARCHAR(255) NULL DEFAULT NULL,
   `username` VARCHAR(99) NULL DEFAULT '',
   `group_name` VARCHAR(255) NULL DEFAULT '',
   `group_id` INT(11) UNSIGNED NULL DEFAULT '0',
   `function` VARCHAR(32) NOT NULL DEFAULT '',
   `permission_id` INT(11) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=MyISAM;





# Replace placeholder table for user_view_permissions with correct view syntax
# ------------------------------------------------------------

DROP TABLE `user_view_permissions`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `user_view_permissions` AS (select `lu`.`user_id` AS `user_id`,`lu`.`email` AS `email`,`lu`.`username` AS `username`,`lg`.`group_name` AS `group_name`,`lg`.`group_id` AS `group_id`,`lp`.`function` AS `function`,`lp`.`permission_id` AS `permission_id` from ((((`user_ls_permissions` `lp` left join `user_rel_permissions` `rp` on((`lp`.`permission_id` = `rp`.`permission_id`))) left join `user_ls_groups` `lg` on((`rp`.`group_id` = `lg`.`group_id`))) left join `user_rel_groups` `rg` on((`lg`.`group_id` = `rg`.`group_id`))) left join `user_ls_users` `lu` on((`rg`.`user_id` = `lu`.`user_id`))));

/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
