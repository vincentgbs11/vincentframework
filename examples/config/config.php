<?php
// CORE FRAMEWORK SETTINGS
define('DEBUG', 'ON');
define('LOG', 'OFF');
define('DOMAIN', 'http://192.168.33.10');

define('FILE', '/var/www/');

define('DBHOST', '127.0.0.1');
define('DBUSER', 'php_framework');
define('DBPASS', 'password');
define('DATABASE', 'php_framework');
date_default_timezone_set('EST');
define('HOME', 'user/user/home');

// OTHER SETTINGS
// available modules for this instance of the application
define('MODULES', ['short', 'user']);
// how to redirect (PRETTY or GET)
define('URLTYPE', 'GET');

// user setting: require https connection (0 or 1)
define('HTTPS', 0);
// user settings: days that a cookie or session are valid
define('LOGINEXPIRE', 30);
// user settings: encryption method and password hash iterations
define('ENCRYPTION', 'PBKDF2');
define('HASHITERATIONS', 999999);

// display domain
define('DISPLAYDOMAIN', 'vincentframework.localhost');
?>
